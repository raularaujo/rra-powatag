﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace PowaTag.Utility
{
    class AssociationUriMapper : UriMapperBase
    {
        private string _tempUri;

        public override Uri MapUri(Uri uri)
        {
            _tempUri = System.Net.HttpUtility.UrlDecode(uri.ToString());

            if (_tempUri.Contains("tagitapp:"))
            {
                // Get the reference ID (after "reference=").
                var split = _tempUri.Split('=');
                //int categoryIdIndex = _tempUri.IndexOf("reference=") + 1;
                //string categoryId = _tempUri.Substring(categoryIdIndex);
                if (split.Length > 2)
                    // Map the show products request to ShowProducts.xaml
                    return new Uri("/Views/SummaryView.xaml?tagurl=" + split[2], UriKind.Relative);
                else
                    //if the split wasn't successful, launch MainPage
                    return new Uri("/Views/MainPage.xaml", UriKind.Relative);
            }

            // Otherwise perform normal launch.
            return uri;
        }
    }

}

