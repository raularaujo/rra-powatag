﻿using PowaTag.Models;
using PowaTag.Utility;
using PowaTag.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PowaTag
{

    /// <summary>
    /// This Class creates sets and generate a WebClient request.
    /// </summary>
    public class RequestClientManager
    {
        #region Properties and Enums
        private string _date;
        /// <summary>
        /// The date and time that the request was sent
        /// </summary>
        public string Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private string _accept;
        /// <summary>
        /// Content-Types that are acceptable for the response
        /// </summary>
        public string Accept
        {
            get { return _accept; }
            set { _accept = value; }
        }

        private string _acceptEncodingType;
        /// <summary>
        /// Acceptable encoding.
        /// </summary>
        public string AcceptEncodingType
        {
            get { return _acceptEncodingType; }
            set { _acceptEncodingType = value; }
        }

        private string _apiKey;
        /// <summary>
        /// This key ensures the app has permission to make the request. Included in the headers of the request.
        /// </summary>
        public string ApiKey
        {
            get { return _apiKey; }
            set { _apiKey = value; }
        }

        private string _contentType;
        /// <summary>
        /// The MIME type of the body of the request (Json, Post method)
        /// </summary>
        public string ContentType
        {
            get { return _contentType; }
            set { _contentType = value; }
        }

        private string _deviceFingerPrint;
        /// <summary>
        /// Unique device id
        /// </summary>
        public string DeviceFingerPrint
        {
            get { return _deviceFingerPrint; }
            set { _deviceFingerPrint = value; }
        }

        private string _finalHMAC;
        /// <summary>
        /// Purchase encrypted information to be send to the server as part of the headers.
        /// </summary>
        public string FinalHMAC
        {
            get { return _finalHMAC; }
            set { _finalHMAC = value; }
        }

        private string _httpMethod;
        /// <summary>
        /// Request Method type: POST for purchase requests.
        /// </summary>
        public string HttpMethod
        {
            get { return _httpMethod; }
            set { _httpMethod = value; }
        }

        private string _page;
        /// <summary>
        ///  Page setting for pagination to retrieve History records.
        /// </summary>
        public string Page
        {
            get { return _page; }
            set { _page = value; }
        }

        private string _referenceId;
        /// <summary>
        ///  Reference ID of the item itself
        /// </summary>
        public string ReferenceId
        {
            get { return _referenceId; }
            set { _referenceId = value; }
        }

        private string _requestBody;
        /// <summary>
        /// Usually a Json structure for the request and response for purchase (start, complete or abort)
        /// </summary>
        public string RequestBody
        {
            get { return _requestBody; }
            set { _requestBody = value; }
        }

        private string _url;
        /// <summary>
        /// Base URL where requests are sent.
        /// </summary>
        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        private Encoding _bodyEncoding = UTF8Encoding.UTF8;
        /// <summary>
        /// Encoding to use for the body of the POST message. The default is UTF8.
        /// </summary>
        public Encoding BodyEncoding
        {
            get { return _bodyEncoding; }
            set { _bodyEncoding = value; }
        }

        //Enum for the end point (Start, Complete, Abort)
        public enum PurchaseEndpoints
        {
            PurchaseStart,
            PurchaseComplete,
            PurchaseAbort,
            AccountSignUp
        }
        #endregion

        //RequestResponse struct used to pass information about the request and response 
        public struct RequestResponse
        {
            public HttpWebRequest WebRequest { get; set; }
            public string RequestBody { get; set; }
            public string Url { get; set; }
        };

        /// <summary>
        /// Fired when a response has been returned.
        /// </summary>
        public event EventHandler<WebResponseArgs> ResponseReturned;
        public event EventHandler<WebResponseArgs> PurchaseStartCompleted;
        public event EventHandler<WebResponseArgs> PurchaseAbortCompleted;
        public event EventHandler<WebResponseArgs> PurchaseCompleteCompleted;
        public event EventHandler<WebResponseArgs> AccountSignupCompleted;
        public event EventHandler<WebResponseArgs> EmailVerificationCompleted;

        public Dictionary<string, object> jsonData = new Dictionary<string, object>();

        /// <summary>
        /// Create a RequestClientManager object which will facilitate calling the endpoints
        /// </summary>
        /// <param name="accept">Content-Types that are acceptable for the response</param>
        /// <param name="acceptEncodingType">Acceptable encoding</param>
        /// <param name="apiKey">A key that ensures the app has permission to make the request</param>
        /// <param name="contentType">>MIME type of the body</param>
        /// <param name="deviceFingerPrint">Unique device ID</param>
        /// <param name="httpMethod">Request Method type: POST for purchase requests.</param>
        public RequestClientManager(string accept, string acceptEncodingType, string apiKey, string contentType,
                                    string deviceFingerPrint, string httpMethod)
        {
            Accept = accept;
            AcceptEncodingType = acceptEncodingType;
            ApiKey = apiKey;
            ContentType = contentType;
            DeviceFingerPrint = deviceFingerPrint;
            HttpMethod = httpMethod;
        }


        //ManualResetEvent allDone = new ManualResetEvent(false);
        private PurchaseEndpoints _requestType;

        /// <summary>
        /// Send a request
        /// </summary>
        /// <param name="pEnd">PurchaeEndPoint to call (e.g. Start, Abort, Complete)</param>
        /// <param name="refId">ReferenceID of the tag</param>
        /// <param name="hmac">HMAC to use for authentication</param>
        /// <param name="lat">Latitude of current position</param>
        /// <param name="lon">Longitude of current position</param>
        public void SendPurchaseRequest(PurchaseEndpoints pEnd, string refId, string hmac, string latitude, string longitude, string cvv = "", string deliveryId = "", string paymentId = "", Dictionary<string, Dictionary<string, Option>> options = null)
        {
            jsonData = new Dictionary<string, object>();
            jsonData.Add("apiKey", ApiKey);
            jsonData.Add("deviceFingerprint", DeviceFingerPrint);
            jsonData.Add("reference", refId);
            jsonData.Add("latitude", latitude);
            jsonData.Add("longitude", longitude);
            _requestType = pEnd;
            string url = "";
            switch (pEnd)
            {
                case PurchaseEndpoints.PurchaseStart:
                    url = Utilities.CPS_ENDPOINT;
                    break;

                case PurchaseEndpoints.PurchaseAbort:
                    url = Utilities.CPA_ENDPOINT;
                    break;

                case PurchaseEndpoints.PurchaseComplete:
                    url = Utilities.CPC_ENDPOINT;

                    jsonData.Add("cvv", cvv);
                    jsonData.Add("deliveryAddressId", deliveryId);
                    jsonData.Add("paymentMethodId", paymentId);

                    // //Used to transmit the options details 
                    // Dictionary<string, Dictionary<string, Option>> optionsDict = new Dictionary<string, Dictionary<string, Option>>();
                    // Dictionary<string, Option> optDict = new Dictionary<string, Option>();

                    // optDict.Add("size", new Option("SS13-FR238-NAVY-S", "Small", "9493610"));

                    //// optionsDict.Add("SS13-FR238-NAVY", optDict); //use to pass options
                    // optionsDict.Add("SS13-FR345-BRACES-STRIPE", null); //use to pass no options

                    jsonData.Add("productOptions", options);

                    break;
            }

            if (String.IsNullOrEmpty(url))
                throw new ArgumentException("A valid endpoint was not chosen");
            else
            {
                try
                {
                    string JSON = Newtonsoft.Json.JsonConvert.SerializeObject(jsonData, Newtonsoft.Json.Formatting.Indented);


                    string reqBody = JSON;// String.Format(JSON, ApiKey, DeviceFingerPrint, refId, latitude, longitude);
                    HttpWebRequest request = GenerateRequest(String.Format(url, ApiKey, DeviceFingerPrint, refId), hmac);
                    //WebClientAbort(String.Format(url, ApiKey, DeviceFingerPrint, refId), hmac, reqBody);

                    // Start the asynchronous request.
                    IAsyncResult result = (IAsyncResult)request.BeginGetRequestStream(new AsyncCallback(ResponseCallback), new RequestResponse() { WebRequest = request, RequestBody = reqBody, Url = url });

                }
                catch (WebException e)
                {
                    //SendCompletedEvent(new WebResponseArgs("ERROR", url));
                    ResponseReturned(this, new WebResponseArgs("ERROR " + e.Message));
                    throw;

                }
                catch (Exception e)
                {
                    //SendCompletedEvent(new WebResponseArgs("ERROR", url));
                    ResponseReturned(this, new WebResponseArgs("ERROR " + e.Message));
                }
            }
        }

        #region SignUp Request
        /// <summary>
        /// Send a request
        /// </summary>
        /// <param name="pEnd">PurchaeEndPoint to call (e.g. Start, Abort, Complete)</param>
        /// <param name="refId">ReferenceID of the tag</param>
        /// <param name="hmac">HMAC to use for authentication</param>
        /// <param name="lat">Latitude of current position</param>
        /// <param name="lon">Longitude of current position</param>
        public void SendAccountSignupRequest(Dictionary<string, object> customParameterFields, string userAgent = "WP")
        {


            jsonData = new Dictionary<string, object>();
            //jsonData.Add("apiKey", ApiKey);
            //jsonData.Add("deviceFingerprint", DeviceFingerPrint);
            //jsonData.Add("reference", refId);
            //jsonData.Add("latitude", latitude);
            //jsonData.Add("longitude", longitude);

            _requestType = PurchaseEndpoints.AccountSignUp;
            string url = "";

            url = Utilities.CSD_ENDPOINT;

            try
            {
                string JSON = Newtonsoft.Json.JsonConvert.SerializeObject(customParameterFields, Newtonsoft.Json.Formatting.Indented);

                string reqBody = JSON;// String.Format(JSON, ApiKey, DeviceFingerPrint, refId, latitude, longitude);
                HttpWebRequest request = GenerateAccountSignUpRequest(String.Format(url, Utilities.API_KEY, Utilities.DEVICE_FINGERPRINT));


                // Start the asynchronous request.
                IAsyncResult result = (IAsyncResult)request.BeginGetRequestStream(new AsyncCallback(ResponseCallback), new RequestResponse() { WebRequest = request, RequestBody = reqBody, Url = url });

            }
            catch (WebException e)
            {
                //SendCompletedEvent(new WebResponseArgs("ERROR", url));
                ResponseReturned(this, new WebResponseArgs("ERROR " + e.Message));
                throw;

            }
            catch (Exception e)
            {
                //SendCompletedEvent(new WebResponseArgs("ERROR", url));
                ResponseReturned(this, new WebResponseArgs("ERROR " + e.Message));
            }

        }


        private HttpWebRequest GenerateAccountSignUpRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)SharpGIS.WebRequestCreator.GZip.Create(new Uri(url, UriKind.Absolute));

            Debug.WriteLine("\n@RequesClientMnager.cs [GenerateAccountSignUpRequest] URL value is: " + url);

            request.Method = HttpMethod;
            request.Headers["apiKey"] = ApiKey;
            Date = DateTime.Now.ToString();
            request.Headers[HttpRequestHeader.Date] = Date;
            request.ContentType = ContentType;
            request.Accept = Accept;
            request.Headers[HttpRequestHeader.AcceptEncoding] = AcceptEncodingType;
            request.Headers["api"] = "application/vnd.com.powatag.client.api.v1.0.0+json";


            return request;
        }
        private void SignUpResponseCallback(IAsyncResult asynchronousResult)
        {
            RequestResponse rr = ((RequestResponse)asynchronousResult.AsyncState);
            HttpWebRequest request = rr.WebRequest;

            try
            {
                // End the operation
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);

                using (Stream streamResponse = response.GetResponseStream())
                {
                    StreamReader streamRead = new StreamReader(streamResponse);
                    string responseString = streamRead.ReadToEnd();

                    // Close the stream object
                    streamResponse.Close();
                    streamRead.Close();

                    // Release the HttpWebResponse
                    response.Close();

                    //fire the completed event
                    // if (ResponseReturned != null)
                    //    ResponseReturned(request, new WebResponseArgs(responseString));
                    SendCompletedEvent(new WebResponseArgs(responseString, rr.Url));
                }

                //reset the ManualEventHandler
                //allDone.Set();
            }
            catch (Exception ex)
            {
                SendCompletedEvent(new WebResponseArgs(ex.Message, rr.Url));
                // ResponseReturned(this, new WebResponseArgs("ERROR", rr.Url));
                // throw;
            }
        }
        #endregion

        #region Account Request
        public void GenerateAccountRequest()
        {
            //Formating the endpoint URL with the Get Parameters-Information
            Uri url = new Uri(string.Format(Utilities.ACCOUNT_ENDPOINT, ApiKey, DeviceFingerPrint));

            Debug.WriteLine("\n@RequesClientMnager.cs [GenerateAccountRequest] URL value is: " + url.AbsoluteUri);

            HttpWebRequest request = (HttpWebRequest)SharpGIS.WebRequestCreator.GZip.Create(url);

            request.Method = HttpMethod;
            request.Headers["apiKey"] = ApiKey;
            //request.Headers[HttpRequestHeader.Date] = date;
            //request.ContentType = ContentType; //ContentType only accepted by POST and PUT Rqst.
            request.Accept = Accept;
            request.Headers[HttpRequestHeader.AcceptEncoding] = AcceptEncodingType;
            request.Headers["api"] = "application/vnd.com.powatag.client.api.v1.0.0+json";

            IAsyncResult result = request.BeginGetResponse(new AsyncCallback(AccountResponseCallback), request);
        }

        private void AccountResponseCallback(IAsyncResult ar)
        {
            HttpWebRequest request = (HttpWebRequest)ar.AsyncState;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(ar);
                using (Stream streamResponse = response.GetResponseStream())
                {
                    StreamReader streamRead = new StreamReader(streamResponse);
                    string responseString = streamRead.ReadToEnd();

                    // Close the stream object
                    streamResponse.Close();
                    streamRead.Close();

                    // Release the HttpWebResponse
                    response.Close();

                    Debug.WriteLine("\n@RequestClientManager [AccountResponseCallback] Data has been recieved \n" + responseString);
                    //fire the completed event
                    if (ResponseReturned != null)
                        ResponseReturned(request, new WebResponseArgs(responseString));
                }
            }
            catch (WebException ex)
            {
                int statusCode = -1;
                HttpWebResponse exResponse = ex.Response as HttpWebResponse;
                if (exResponse != null)
                {
                    using (StreamReader reader = new StreamReader(exResponse.GetResponseStream()))
                    {
                        Debug.WriteLine("\n@RequestClientManager [AccountResponseCallback] The Error's content: " + reader.ReadToEnd());
                    }
                    statusCode = (int)exResponse.StatusCode;
                    Debug.WriteLine("\n@RequestClientManager [AccountResponseCallback] Error Status Code is: " + statusCode.ToString());
                }
            }
        }
        #endregion

        #region History Records Request
        /// <summary>
        /// Generates a Http request to the server requesting the History Records.
        /// </summary>
        /// <param name="hmac">Hmac generated by the app</param>
        /// <param name="date">Request date</param>
        /// <param name="page">Page setting for pagination to retrieve History records.</param>
        /// <param name="size">Number of items in a page to retrieve</param>
        public void GenerateHistoryRecordsRequest(string date, string page, string size)
        {
            //Formating the endpoint URL with the Get Parameters-Information
            Uri url = new Uri(string.Format(Utilities.HISTORY_ENDPOINT, date, ApiKey, DeviceFingerPrint, page, size));

            Debug.WriteLine("\n@RequesClientMnager.cs [GenerateHistoryRecordsRequest] URL value is: " + url.AbsoluteUri);

            HttpWebRequest request = (HttpWebRequest)SharpGIS.WebRequestCreator.GZip.Create(url);

            request.Method = HttpMethod;
            request.Headers["apiKey"] = ApiKey;
            //request.Headers["Authorization"] = hmac;
            request.Headers[HttpRequestHeader.Date] = date;
            //request.ContentType = ContentType; //ContentType only accepted by POST and PUT Rqst.
            request.Accept = Accept;
            request.Headers[HttpRequestHeader.AcceptEncoding] = AcceptEncodingType;
            request.Headers["api"] = "application/vnd.com.powatag.client.api.v1.0.0+json";

            IAsyncResult result = request.BeginGetResponse(new AsyncCallback(HistoryResponseCallback), request);
        }

        /// <summary>
        /// Asynchronous callback to receive Sever's response. Response should be a JSON with the History Records.
        /// </summary>
        /// <param name="ar">The HttpWebRequest object used to create the request.</param>
        private void HistoryResponseCallback(IAsyncResult ar)
        {
            HttpWebRequest request = (HttpWebRequest)ar.AsyncState;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(ar);
                using (Stream streamResponse = response.GetResponseStream())
                {
                    StreamReader streamRead = new StreamReader(streamResponse);
                    string responseString = streamRead.ReadToEnd();

                    // Close the stream object
                    streamResponse.Close();
                    streamRead.Close();

                    // Release the HttpWebResponse
                    response.Close();

                    Debug.WriteLine("\n@RequestClientManager [HistoryResponseCallback] Data has been recieved \n" + responseString);
                    //fire the completed event
                    if (ResponseReturned != null)
                        ResponseReturned(request, new WebResponseArgs(responseString));
                }
            }
            catch (WebException ex)
            {
                int statusCode = -1;
                HttpWebResponse exResponse = ex.Response as HttpWebResponse;
                if (exResponse != null)
                {
                    using (StreamReader reader = new StreamReader(exResponse.GetResponseStream()))
                    {
                        Debug.WriteLine("\n@RequestClientManager [HistoryResponseCallback] The Error's content: " + reader.ReadToEnd());
                    }
                    statusCode = (int)exResponse.StatusCode;
                    Debug.WriteLine("\n@RequestClientManager [HistoryResponseCallback] Error Status Code is: " + statusCode.ToString());
                }
            }
        }
        #endregion

        #region Email Verification
        private static ManualResetEvent allDone = new ManualResetEvent(false);

        public void EmailVerification(string email)
        {
            string url = String.Format("https://integration.powatag.com/client/account/user/{0}", email);

            WebRequest request = WebRequest.Create(url);
            request.Method = "GET";


            var result = request.BeginGetResponse(new AsyncCallback(VerificationCallback), new RequestResponse() { Url = url, WebRequest = (HttpWebRequest)request, RequestBody = string.Empty });

        }


        private void VerificationCallback(IAsyncResult asynchronousResult)
        {
            RequestResponse rr = ((RequestResponse)asynchronousResult.AsyncState);
            HttpWebRequest request = rr.WebRequest;

            try
            {
                // End the operation
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    SendVerificationEvent(new WebResponseArgs("OK", rr.Url));
                    return;
                }

            }
            catch (WebException we)
            {
                using (WebResponse response = we.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    if (httpResponse.StatusCode == HttpStatusCode.BadRequest)
                    {

                        SendVerificationEvent(new WebResponseArgs("BadRequest", rr.Url));
                        return;
                    }
                    if (httpResponse.StatusCode == HttpStatusCode.NotFound)
                    {

                        SendVerificationEvent(new WebResponseArgs("NotFound", rr.Url));
                        return;
                    }
                }
            }
            catch (Exception)
            {
                return;
                //ignore
                //SendCompletedEvent(new WebResponseArgs(ex.Message, rr.Url));
            }
        }
        /// <summary>
        /// Fire the relevant 
        /// </summary>
        /// <param name="e"></param>
        private void SendVerificationEvent(WebResponseArgs e)
        {
            if (e.Response.Equals("OK"))
            {
                if (EmailVerificationCompleted != null)
                    EmailVerificationCompleted(this, e);
            }
            else if (e.Response.Equals("BadRequest"))
            {
                if (EmailVerificationCompleted != null)
                    EmailVerificationCompleted(this, e);
            }
            else if (e.Response.Equals("NotFound"))
            {
                if (EmailVerificationCompleted != null)
                    EmailVerificationCompleted(this, e);
            }


        }
        #endregion

        private HttpWebRequest GenerateRequest(string url, string hmac)
        {
            HttpWebRequest request = (HttpWebRequest)SharpGIS.WebRequestCreator.GZip.Create(new Uri(url, UriKind.Absolute));

            request.Method = HttpMethod;
            request.Headers["apiKey"] = ApiKey;
            request.Headers["auth"] = hmac;
            Date = DateTime.Now.ToString();
            request.Headers[HttpRequestHeader.Date] = Date;
            request.ContentType = ContentType;
            request.Accept = Accept;
            request.Headers[HttpRequestHeader.AcceptEncoding] = AcceptEncodingType;
            request.Headers["api"] = "application/vnd.com.powatag.client.api.v1.0.0+json";


            return request;
        }


        private void ResponseCallback(IAsyncResult asyncResult)
        {
            string calledUrl = "";

            try
            {
                RequestResponse rr = (RequestResponse)asyncResult.AsyncState;
                calledUrl = rr.Url;
                //Get the request and add the body (JSON data that needs to be sent to the server)
                //HttpWebRequest request = (HttpWebRequest)asyncResult.AsyncState;


                byte[] byteArray = Encoding.UTF8.GetBytes(rr.RequestBody);
                using (Stream postStream = rr.WebRequest.EndGetRequestStream(asyncResult))
                {
                    postStream.Write(byteArray, 0, byteArray.Length);
                    postStream.Flush();
                    postStream.Close();
                }
                rr.WebRequest.BeginGetResponse(new AsyncCallback(GetResponseCallback), rr);
                //rr = default(RequestResponse);

            }
            catch (Exception ex)
            {
                SendCompletedEvent(new WebResponseArgs("ERROR " + ex.Message, calledUrl));
                //ResponseReturned(this, new WebResponseArgs("ERROR"));

            }
        }

        private void GetResponseCallback(IAsyncResult asynchronousResult)
        {
            //  private static ManualResetEvent allDone = new ManualResetEvent(false);
            //HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
            RequestResponse rr = ((RequestResponse)asynchronousResult.AsyncState);
            HttpWebRequest request = rr.WebRequest;

            try
            {
                // End the operation
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);

                using (Stream streamResponse = response.GetResponseStream())
                {
                    StreamReader streamRead = new StreamReader(streamResponse);
                    string responseString = streamRead.ReadToEnd();

                    // Close the stream object
                    streamResponse.Close();
                    streamRead.Close();

                    // Release the HttpWebResponse
                    response.Close();

                    //fire the completed event
                    // if (ResponseReturned != null)
                    //    ResponseReturned(request, new WebResponseArgs(responseString));
                    SendCompletedEvent(new WebResponseArgs(responseString, rr.Url));
                }

                //reset the ManualEventHandler
                //allDone.Set();
            }
            catch (WebException we)
            {
                using (WebResponse response = we.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        Console.WriteLine(text);
                    }
                }
                SendCompletedEvent(new WebResponseArgs(we.Message, rr.Url));
            }
            catch (Exception ex)
            {
                SendCompletedEvent(new WebResponseArgs(ex.Message, rr.Url));
                // ResponseReturned(this, new WebResponseArgs("ERROR", rr.Url));
                // throw;
            }
        }

        /// <summary>
        /// Fire the relevant 
        /// </summary>
        /// <param name="e"></param>
        private void SendCompletedEvent(WebResponseArgs e)
        {
            if (e.Url == Utilities.CPS_ENDPOINT)
            {
                if (PurchaseStartCompleted != null)
                    PurchaseStartCompleted(this, e);
            }
            else if (e.Url == Utilities.CPC_ENDPOINT)
            {
                if (PurchaseCompleteCompleted != null)
                    PurchaseCompleteCompleted(this, e);
            }
            else if (e.Url == Utilities.CPA_ENDPOINT)
            {
                if (PurchaseAbortCompleted != null)
                    PurchaseAbortCompleted(this, e);
            }
            else if (e.Url == Utilities.CSD_ENDPOINT)
            {
                if (AccountSignupCompleted != null)
                    AccountSignupCompleted(this, e);
            }


        }
    }
}
