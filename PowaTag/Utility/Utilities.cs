﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PowaTag.Utility
{
    public static class Utilities
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">API_KEY + DEVICE_FINGERPRINT + SECRET_KEY + REFERENCE_ID</param>
        /// <param name="secret">SECRET</param>
        /// <returns></returns>
        public static string GetHMAC(string message, string secret)
        {
            // string msg = API_KEY + DEVICE_FINGERPRINT + SECRET_KEY + REFERENCE_ID;

            byte[] keyBytes = UTF8Encoding.UTF8.GetBytes(secret);
            byte[] messageBytes = UTF8Encoding.UTF8.GetBytes(message);


            HMACSHA256 hmac = new HMACSHA256(keyBytes);

            hmac.Initialize();

            string final = Convert.ToBase64String(hmac.ComputeHash(messageBytes));

            return final;
        }

        #region EndPoints

        public static string PowaTagUrl = "http://www.powatag.com";// /index.html

        public static string ENDPOINT_QUERYSTIRNG = "?apiKey={0}&deviceFingerprint={1}&reference={2}";
        //Dev

        //public static string CPS_ENDPOINT = "https://dev.powatag.com/CPS?apiKey={0}&deviceFingerprint={1}&reference={2}"; //start
        //public static string CPA_ENDPOINT = "https://dev.powatag.com/CPA?apiKey={0}&deviceFingerprint={1}&reference={2}"; //abort       
        //public static string CPC_ENDPOINT = "https://dev.powatag.com/CPC?apiKey={0}&deviceFingerprint={1}&reference={2}"; //complete

        public static string CSD_ENDPOINT = "https://dev.powatag.com/client/signup/direct?apiKey={0}&deviceFingerprint={1}"; //signup
        public static string CPS_ENDPOINT = "https://dev.powatag.com/CPS?apiKey={0}&deviceFingerprint={1}&reference={2}"; //start
        public static string CPA_ENDPOINT = "https://dev.powatag.com/CPA?apiKey={0}&deviceFingerprint={1}&reference={2}"; //abort       
        public static string CPC_ENDPOINT = "https://dev.powatag.com/CPC?apiKey={0}&deviceFingerprint={1}&reference={2}"; //complete
        public static string CAU_ENDPOINT = "https://integration.powatag.com/user/client/account/{0}"; //verify email address ({0} replaced with email address to test)

        public static string HISTORY_ENDPOINT = "http://dev.powatag.com/client/user/orders?date={0}&apiKey={1}&deviceFingerprint={2}&page={3}&size={4}";
        public static string ACCOUNT_ENDPOINT = "http://demo.powatag.com/client/account/user?apiKey={0}&deviceFingerprint={1}";

        //Demo instance

        //public static string CPS_ENDPOINT = "https://demo.powatag.com/CPS?apiKey={0}&deviceFingerprint={1}&reference={2}"; //start
        //public static string CPA_ENDPOINT = "https://demo.powatag.com/CPA?apiKey={0}&deviceFingerprint={1}&reference={2}"; //abort       
        //public static string CPC_ENDPOINT = "https://demo.powatag.com/CPC?apiKey={0}&deviceFingerprint={1}&reference={2}"; //complete

        //public static string CSD_ENDPOINT = "https://demo.powatag.com/client/signup/direct?apiKey={0}&deviceFingerprint={1}"; //start
        //public static string CPS_ENDPOINT = "https://demo.powatag.com/CPS?apiKey={0}&deviceFingerprint={1}&reference={2}"; //start
        //public static string CPA_ENDPOINT = "https://demo.powatag.com/CPA?apiKey={0}&deviceFingerprint={1}&reference={2}"; //abort       
        //public static string CPC_ENDPOINT = "https://demo.powatag.com/CPC?apiKey={0}&deviceFingerprint={1}&reference={2}"; //complete

        //public static string HISTORY_ENDPOINT = "http://demo.powatag.com/client/user/orders?date={0}&apiKey={1}&deviceFingerprint={2}&page={3}&size={4}";
        //public static string ACCOUNT_ENDPOINT = "http://demo.powatag.com/client/account/user?apiKey={0}&deviceFingerprint={1}";
        #endregion

        public static string API_KEY = "9c497051-9382-4349-a60b-5f220bb07562";
        public static string DEVICE_FINGERPRINT = "82cfb85953445fe1f40d5fd23065a4766e9491de5e9d1b30769e76903ca3fe62";
        public static string REFERENCE_ID; // = "3f91d9de0762103f586f3c8c3b7396b468035afa24e480c30d7e737decc01140"; //always works!
        public static string SECRET_KEY = "ACDEF01234";
        

        /// <summary>
        /// Retrieves the URI of the last page in the Navigation stack. (Returns an empty string if a previous page is not available)
        /// </summary>
        /// <returns></returns>
        public static string LastPageInNavigationStack()
        {
            var frame = System.Windows.Application.Current.RootVisual as Microsoft.Phone.Controls.PhoneApplicationFrame;
            if (frame != null)
            {
                return frame.BackStack.FirstOrDefault().Source.ToString();
            }
            else
                return String.Empty;

        }

        //public static string HISTORY_ENDPOINT = "http://demo.powatag.com/client/user/orders?date={0}&apiKey={1}&deviceFingerprint={2}&page={3}&size={4}";
        //public static string ACCOUNT_ENDPOINT = "http://demo.powatag.com/client/account/user?apiKey={0}&deviceFingerprint={1}";


        /// <summary>
        /// Coordinates to adjust the map marker image location on the map, so it points to the desired location
        /// </summary>
        public static readonly Point MapMarkerOrigin = new Point(0.3, 0.98);

 
        public static bool IsNetworkAvailable = true;

        //Move to resource file
        //public static string NetworkErrorMessage = "There's a problem connecting to the server. Please ensure you have network connectivity";
 
        public static readonly string IsoFolderName = "PowaTag";
        public static string AccountTimeStampSetting = "accTimeStamp";
        public static string AccountDataFileName = "accountinfo.txt";
        public static string HistoryTimeStampSetting = "histTimeStamp";
        public static string HistoryDataFileName = "historyInfo.txt";
        public static string Build = "pt 0.1.0";
        public static string BuildDate = "Oct 25 2013";
        public static string PowaFacebook = "TagIt";
        public static string PowaTwitter = "@powatag";

        
 

        #region Map properties
        public static string ImagerySet { get { return "Road"; } }
        public static string CenterPoint { get; set; }
        public static string ZoomLevel { get { return "16"; } }
        public static string PushpinPoint { get; set; }
        public static string MapSize { get { return "430,200"; } }
        public static string IconStyle { get { return "46"; } }

        public static string MapKey { get { return "AhZ3ZsFaQ2WxEmdEQ_FTBARBA1AXhKDFowvUyjG588uSqEjcR_4bhcKbgh7ozOkn"; } }
        public static string StaticImageryMapUrl { get { return "http://dev.virtualearth.net/REST/v1/Imagery/Map/{0}/{1}/{2}?mapSize={3}&pushpin={4};{5}&key={6}"; } }

        #endregion
    }
}
