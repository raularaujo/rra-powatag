﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.Utility
{
    public static class StorageManager
    {
        /// <summary>
        /// Settings of this app
        /// </summary>
        static IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

        /// <summary>
        /// This method checks whether a setting exists, or not, in the Settings system.
        /// </summary>
        /// <param name="setting">The setting to check</param>
        /// <returns>True or False is the setting exists, or not</returns>
        public static bool CheckSetting(string setting)
        {
            if (settings.Contains(setting))
                return true;
            else
                return false;
        }

        /// <summary>
        /// This method writes a new setting to the Setting System
        /// </summary>
        /// <param name="setting"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static void WriteSettingToSettings(string setting, object value)
        {
                IsolatedStorageSettings.ApplicationSettings[setting] = value;
                IsolatedStorageSettings.ApplicationSettings.Save();
        }

        public static object ReadSettingFromSettings(string setting)
        {
            object settingValue = null;

            if (CheckSetting(setting))
            {
                settingValue = IsolatedStorageSettings.ApplicationSettings[setting];
            }

            return settingValue;
        }

        /// <summary>
        /// Method to save a file to Isolated Storage
        /// </summary>
        /// <param name="fileName">The file name</param>
        /// <param name="content">The content to be saved in the file</param>
        /// <returns></returns>
        public static bool WriteFileToStorage(string fileName, string content)
        {
            bool succeed = false;
            IsolatedStorageFile localFile = IsolatedStorageFile.GetUserStoreForApplication();

            //Check the folder.
            if(!localFile.DirectoryExists(Utilities.IsoFolderName))
                localFile.CreateDirectory(Utilities.IsoFolderName);

            string path = string.Format("{0}\\{1}",Utilities.IsoFolderName,fileName);
            
            //Create file
            try
            {
                using (var isoFileStream = new IsolatedStorageFileStream(path, System.IO.FileMode.Create, localFile))
                {
                    //Write content.
                    using (System.IO.StreamWriter isoFileWriter = new System.IO.StreamWriter(isoFileStream))
                    {
                        isoFileWriter.Write(content);
                        succeed = true;
                        isoFileWriter.Flush();
                        isoFileWriter.Close();
                    }
                    isoFileStream.Close();
                }
            }
            catch (Exception ex)
            {
                succeed = false;
            }

            return succeed;
        }


        public static string ReadFileFromStorage(string filename)
        {
            string content = string.Empty;
            string path = string.Format("{0}\\{1}",Utilities.IsoFolderName,filename);
            IsolatedStorageFile localFile = IsolatedStorageFile.GetUserStoreForApplication();

            if (localFile.FileExists(path))
            {
                using (IsolatedStorageFileStream isoFileStream = new IsolatedStorageFileStream(path, System.IO.FileMode.Open, localFile))
                {
                    using (System.IO.StreamReader isoFileReader = new System.IO.StreamReader(isoFileStream))
                    {
                        content = isoFileReader.ReadToEnd();
                        isoFileReader.Close();
                    }
                    isoFileStream.Close();
                }
            }
            return content;
        }
    }
}
