﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.Utility
{
    public static class PINUtilities
    {
        /// <summary>
        /// Save the PIN to the file.
        /// </summary>
        /// <param name="pin">PIN to save</param>
        /// <returns></returns>
        public static bool SavePIN(string pin)
        {
            //Since this is called by the designer (and an error is thrown as it's in designer), only perform this method if it's not in VS designer
            if (!System.ComponentModel.DesignerProperties.IsInDesignTool)
            {
                try
                {
                    byte[] pinData = Encoding.UTF8.GetBytes(pin); //get the pin into a byte array
                    byte[] protectedPin = ProtectedData.Protect(pinData, null); //Protect the pin and get the return, encrypted, PIN
                    SavePINToFile(protectedPin); //Store the new encrypted PIN to the IsolatedStorage
                    return true;

                }
                catch
                {
                    return false;
                }
            }
            else
                return false;
        }

        /// <summary>
        /// Performs a check to see if the PIN that was entered matches the PIN saved
        /// </summary>
        /// <param name="pin">Entered PIN</param>
        /// <returns></returns>
        public static bool CheckPIN(string pin)
        {

            try
            {
                //temporarily set 123 as the pin if not pin has been entered
                if (!DoesPINExist())
                {
                    SavePIN("123");
                }

                byte[] realPinBytes = LoadPIN();
                byte[] decodedPin = ProtectedData.Unprotect(realPinBytes, null);
                string realPin = Encoding.UTF8.GetString(decodedPin, 0, decodedPin.Length);
                return pin.Equals(realPin); //case-sensitive check
            }
            catch
            {
                return false;
            }

        }

        /// <summary>
        /// LoadPIN is used to load the pin stored on the phone using the Data Protection API
        /// </summary>
        /// <returns>The PIN in bytes (UTF-8 encoded)</returns>
        private static byte[] LoadPIN()
        {

            try
            {
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (store.FileExists(pinFilePath))
                    {
                        using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream(pinFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read, store))
                        using (var reader = new StreamReader(stream).BaseStream)
                        {
                            byte[] pinArray = new byte[reader.Length];
                            reader.Read(pinArray, 0, pinArray.Length);
                            return pinArray;
                        }
                    }
                    else
                        return new byte[0];
                }
            }
            catch
            {
                return new byte[0];
            }


        }


        /// <summary>
        /// Check to see if a PIN has been stored
        /// </summary>
        /// <returns></returns>
        public static bool DoesPINExist()
        {
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                return store.FileExists(pinFilePath);
            }

        }

        /// <summary>
        /// Remove the PIN
        /// </summary>
        /// <returns>True if the PIN was successfully removed. GOOD IDEA TO ASK FOR USER TO ENTER THEIR PIN BEFORE CALLING THIS METHOD</returns>
        public static bool RemovePIN()
        {

            try
            {
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    store.DeleteFile(pinFilePath);
                    return true;
                }
            }
            catch
            {
                return false;
            }

        }

        //Where to store the PIN password file
        private static string pinFilePath = "PinFile";

        /// <summary>
        /// This method saves the pin using the Data Protection API
        /// </summary>
        /// <param name="pinData">The PIN to save</param>
        private static void SavePINToFile(byte[] pinData)
        {

            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream(pinFilePath, System.IO.FileMode.Create, System.IO.FileAccess.Write, store))
            using (var writer = new StreamWriter(stream).BaseStream)
                writer.Write(pinData, 0, pinData.Length);


        }
    }
}
