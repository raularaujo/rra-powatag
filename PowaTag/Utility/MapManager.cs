﻿using PowaTag.Geolocation;
using PowaTag.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.Utility
{
    class MapManager
    {
        /// <summary>
        /// Event Handler to be fired when the Device is Located
        /// </summary>
        public EventHandler<LocationCompletedArgs> ImageryMapLocationEvent;
        
        public MapManager() { }

        /// <summary>
        /// Following the specification for requesting a Static Map Imagery at a specified point, this method formats the URL needed to make said request.
        /// More info http://msdn.microsoft.com/en-us/library/ff701724.aspx
        /// </summary>
        /// <param name="centerPoint"></param>
        /// <param name="pushpinPoint"></param>
        /// <returns></returns>
        public string GetStaticImageryMap(string centerPoint, string pushpinPoint)
        {
            Utilities.CenterPoint = centerPoint;
            Utilities.PushpinPoint = pushpinPoint;

            string finalStaticMapImageryUrl = string.Format(Utilities.StaticImageryMapUrl, Utilities.ImagerySet, Utilities.CenterPoint, Utilities.ZoomLevel, Utilities.MapSize, Utilities.PushpinPoint, Utilities.IconStyle, Utilities.MapKey);
            return finalStaticMapImageryUrl;
        }

        /// <summary>
        /// This method call asynchronously the GetCurrentLocation method from the TagGeolocator class to locate de device.
        /// </summary>
        public async void GetCurrentLocation()
        {
            string location = "0:0";
            try
            {
                //Location services
                location = await TagGeolocator.GetCurrentLocation();

                //Fire the Event when the device is located.
                ImageryMapLocationEvent(this, new LocationCompletedArgs(location.Split(':')[0] + "," + location.Split(':')[1]));
            }
            catch (UnauthorizedAccessException)
            {
                //System.Windows.MessageBox.Show("Location is disabled in phone settings.");
                System.Windows.MessageBox.Show(Resources.AppResources.LocationDisabled);
            }
            catch (Exception ex)
            {
                //TODO Add a place where the user can configure this option.
                //System.Windows.MessageBox.Show("This app has not user consent to use Location services.");
                System.Windows.MessageBox.Show(Resources.AppResources.LocationPermissionDenied);
            }
            
        }
    }

   
}
