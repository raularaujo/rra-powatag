﻿using Microsoft.Phone.Controls;
using PowaTag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace PowaTag
{
    public class PowaNavigationService : INavigationService
    {
        private PhoneApplicationFrame _frame;
        

        public event NavigatingCancelEventHandler Navigating;

        public void NavigateTo(Uri pageUri)
        {
            if(CheckFrame())
                _frame.Navigate(pageUri);    
        }

        public void GoBack()
        {

            if(_frame.CanGoBack && CheckFrame())
                _frame.GoBack();
        }

        public void GoForward()
        {
            if (_frame.CanGoForward && CheckFrame())
                _frame.GoForward();
        }

      

        private bool CheckFrame()
        {
            if (_frame != null)
                return true;

            _frame = Application.Current.RootVisual as PhoneApplicationFrame;
            
            if (_frame != null)
            {
                _frame.Navigating += (s, e) =>
                    {
                        if (Navigating != null)
                            Navigating(s, e);
                    };

                return true;
            }

            return false;
        }
    }
}
