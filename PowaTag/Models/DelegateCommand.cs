﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PowaTag
{
    public class DelegateCommand : ICommand
    {
      
        private Func<object, bool> _canEx;
     
        private Action<object> _exAction;

        private bool _canExCache;

      
        public DelegateCommand(Action<object> act, Func<object, bool> ce)
        {
            this._exAction = act;
            this._canEx = ce;
        }

        public bool CanExecute(object parameter)
        {
            bool tmp = _canEx(parameter);
            if (tmp != _canExCache)
            {
                _canExCache = tmp;
                if (CanExecuteChanged != null)
                    CanExecuteChanged(this, EventArgs.Empty);
            }
            return _canExCache;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            _exAction(parameter);
        }
    }
}
