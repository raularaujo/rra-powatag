﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace PowaTag
{
    public class NavigationServiceCustom : INavigationService
    {
        private PhoneApplicationFrame frame;
        

        public event NavigatingCancelEventHandler Navigating;

        public void NavigateTo(Uri pageUri)
        {
            if(FrameExists())
                frame.Navigate(pageUri);    
        }

        public void GoBack()
        {

            if(frame.CanGoBack && FrameExists())
                frame.GoBack();
        }

        public void GoForward()
        {
            if (frame.CanGoForward && FrameExists())
                frame.GoForward();
        }

        private bool FrameExists()
        {
            if (frame != null)
                return true;

            frame = Application.Current.RootVisual as PhoneApplicationFrame;

            if (frame != null)
            {
                frame.Navigating += (s, e) =>
                    {
                        if (Navigating != null)
                            Navigating(s, e);
                    };

                return true;
            }

            return false;
        }
    }
}
