﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.Models
{
    /// <summary>
    /// This class handles the event went the location process is completed
    /// </summary>
    public class LocationCompletedArgs : EventArgs
    {
        public string Location { get; private set; }

        public LocationCompletedArgs(string location)
        {
            Location = location;
        }
    }
}
