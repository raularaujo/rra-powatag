﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.Models
{
    public class Location
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
    }

    public class HistoryOrder
    {
        public int Id { get; set; }
        public object CreatedDateTime { get; set; }
        public object UpdatedDateTime { get; set; }
        public string PowaTagReference { get; set; }
        public string PowaTagType { get; set; }
        public Shop Shop { get; set; }
        public IEnumerable<Product> Products { get; set; }
        public string OrderStatus { get; set; }
        public User User { get; set; }
        public Device Device { get; set; }
        public Location Location { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public Address DeliveryAddress { get; set; }
        public string Message { get; set; }
        public string ShopIntegrationOrderReference { get; set; }
        public object ShopIntegrationPaymentMethodId { get; set; }
        public object ShopIntegrationShippingMethodId { get; set; }
    }

    public class HistoryRootObject
    {
        public IEnumerable<HistoryOrder> Orders { get; set; }
    }
}
