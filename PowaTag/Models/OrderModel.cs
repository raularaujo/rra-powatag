using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.Models
{
    /// <summary>
    /// Base Class for the product details:
    ///  - Attributes
    ///  - Images
    ///  - Options
    /// </summary>
    public abstract class SelectableBase
    {
        public string UpdatedDateTime { get; set; }
        public string CreatedDateTime { get; set; }
        public string Name { get; set; }
    }

    /// <summary>
    /// Product Option class for calls to CPC
    /// </summary>
    public struct Option
    {
        public string code;
        public string name;
        public string productOptionMappedId;

        public Option(string prodcode, string prodname, string mapped)
        {
            code = prodcode;
            name = prodname;
            productOptionMappedId = mapped;
        }
    };

    #region "Auto-Generated" based on JSON data
    public class Authority
    {
        public string Id { get; set; }
        public string Permission { get; set; }
    }

    public class Group
    {
        public IEnumerable<Authority> Authorities { get; set; }
        public string GroupName { get; set; }
        public string Id { get; set; }
    }
    
    public class User
    {
        public string AccountNonExpired { get; set; }
        public string AccountNonLocked { get; set; }
        public IEnumerable<Address> Addresses { get; set; }
        public string ApiKey { get; set; }
        public string CreatedDateTime { get; set; }
        public string CredentialsNonExpired { get; set; }
        public IEnumerable<string> DateOfBirth { get; set; }
        public IEnumerable<Device> Devices { get; set; }
        public string EmailAddress { get; set; }
        public string Enabled { get; set; }
        public FavouriteDeliveryAddress FavouriteDeliveryAddress { get; set; }
        public FavouritePaymentMethod FavouritePaymentMethod { get; set; }
        public string Gender { get; set; }
        public IEnumerable<Group> Groups { get; set; }
        public string Id { get; set; }
        public string Locale { get; set; }
        public string Name { get; set; }
        public IEnumerable<PaymentMethod> PaymentMethods { get; set; }
        public string Signin { get; set; }
        public string SigninAttempt { get; set; }
        public string TermsAndConditionsAccepted { get; set; }
        public string UpdatedDateTime { get; set; }
    }

    public class Order
    {
        public string CreatedDateTime { get; set; }
        public string DeliveryAddress { get; set; }
        public string Device { get; set; }
        public string Id { get; set; }
        public string Location { get; set; }
        public string Message { get; set; }
        public string OrderStatus { get; set; }
        public string PaymentMethod { get; set; }
        public string PowaTagReference { get; set; }
        public string PowaTagType { get; set; }
        public IEnumerable<Product> Products { get; set; }
        public Shop Shop { get; set; }
        public string ShopIntegrationOrderReference { get; set; }
        public string UpdatedDateTime { get; set; }
        public User User { get; set; }
    }

    public class RootObject
    {
        public Order Order { get; set; }
        public string PurchaseAbortEndpoint { get; set; }
        public string PurchaseCompleteEndpoint { get; set; }
    }

    public class ONLINE
    {
        public int Id { get; set; }
        public long CreatedDateTime { get; set; }
        public long UpdatedDateTime { get; set; }
        public string ChargeType { get; set; }
        public int ChargeRate { get; set; }
        public int UpperBound { get; set; }
        public int LowerBound { get; set; }
    }

    public class OFFLINE
    {
        public int Id { get; set; }
        public long CreatedDateTime { get; set; }
        public long UpdatedDateTime { get; set; }
        public string ChargeType { get; set; }
        public int ChargeRate { get; set; }
        public int UpperBound { get; set; }
        public int LowerBound { get; set; }
    }

    public class AUDIO
    {
        public int Id { get; set; }
        public long CreatedDateTime { get; set; }
        public long UpdatedDateTime { get; set; }
        public string ChargeType { get; set; }
        public int ChargeRate { get; set; }
        public int UpperBound { get; set; }
        public int LowerBound { get; set; }
    }

    public class PowaTagFeeModels
    {
        public ONLINE ONLINE { get; set; }
        public OFFLINE OFFLINE { get; set; }
        public AUDIO AUDIO { get; set; }
    }

    public class ShopIntegrationSettings
    {
        public int Id { get; set; }
        public long CreatedDateTime { get; set; }
        public long UpdatedDateTime { get; set; }
        public string Integration { get; set; }
        public string ExternalShopId { get; set; }
        public string PaymentMethodId { get; set; }
        public string ShippingMethodId { get; set; }
        public string PurchaseCompleteUrl { get; set; }
    }

    public class Shop
    {
        public int Id { get; set; }
        public long CreatedDateTime { get; set; }
        public long UpdatedDateTime { get; set; }
        public string ApiKey { get; set; }
        public string Name { get; set; }
        public bool ImmediateCheckout { get; set; }
        public bool AddToBasket { get; set; }
        public PowaTagFeeModels PowaTagFeeModels { get; set; }
        public ShopIntegrationSettings ShopIntegrationSettings { get; set; }
        public bool AccountNonExpired { get; set; }
        public bool AccountNonLocked { get; set; }
        public bool CredentialsNonExpired { get; set; }
        public bool Enabled { get; set; }
    }

    public class ProductImage : SelectableBase
    {
        public string Url { get; set; }
        public int SortPos { get; set; }
        public bool S3processed { get; set; }
        public string S3Url { get; set; }
    }


    public class ProductOptionValue
    {
        public object Id { get; set; }
        public object CreatedDateTime { get; set; }
        public object UpdatedDateTime { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int SortPos { get; set; }
    }

    public class ProductOption : SelectableBase
    {
        public object MappedId { get; set; }
        public IEnumerable<ProductOptionValue> ProductOptionValues { get; set; }
        public object SelectedProductOptionValue { get; set; }
    }

    public class ProductAttribute : SelectableBase
    {

        public string Value { get; set; }
        public int SortPos { get; set; }
    }

    public class Product
    {
        public object Id { get; set; }
        public object CreatedDateTime { get; set; }
        public object UpdatedDateTime { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public IEnumerable<ProductImage> ProductImages { get; set; }
        public IEnumerable<ProductOption> ProductOptions { get; set; }
        public IEnumerable<ProductAttribute> ProductAttributes { get; set; }
        public IEnumerable<object> ProductUnits { get; set; }
        public string UnitCode { get; set; }
    }

    public class Device
    {
        public int Id { get; set; }
        public long CreatedDateTime { get; set; }
        public long UpdatedDateTime { get; set; }
        public string FriendlyName { get; set; }
        public string Fingerprint { get; set; }
        public string BrandName { get; set; }
        public string ModelName { get; set; }
        public string MarketingName { get; set; }
        public object ModelExtraInfo { get; set; }
        public string DeviceOS { get; set; }
        public string DeviceOSVersion { get; set; }
    }

    public class Country
    {
        public int Id { get; set; }
        public string Alpha2Code { get; set; }
        public string Alpha3Code { get; set; }
        public string Name { get; set; }
        public int NumericCode { get; set; }
    }

    public class Address : SelectableBase
    {
        public int Id { get; set; }
        //public object CreatedDateTime { get; set; }
        //public object UpdatedDateTime { get; set; }
        public string FriendlyName { get; set; }
        //public string Name { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public string Line4 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
        public Country Country { get; set; }
    }

    public class FavouriteDeliveryAddress : SelectableBase
    {
        public int Id { get; set; }
        //public long CreatedDateTime { get; set; }
        //public long UpdatedDateTime { get; set; }
        public string FriendlyName { get; set; }
        //public string Name { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public string Line4 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
        public Country Country { get; set; }
    }

    public class PaymentCard
    {
        public int Id { get; set; }
        public object CreatedDateTime { get; set; }
        public object UpdatedDateTime { get; set; }
        public bool Enabled { get; set; }
        public string FriendlyName { get; set; }
        public string Brand { get; set; }
        public object PrimaryAccountNumber { get; set; }
        public object ShortPrimaryAccountNumber { get; set; }
        public int IssueNumber { get; set; }
        public string NameOnCard { get; set; }
        public object ValidFrom { get; set; }
        public string ExpiresEnd { get; set; }
        public object Cvv { get; set; }
        public Address Address { get; set; }
    }

    public class PaymentMethod : SelectableBase
    {
        public int Id { get; set; }
        //public object CreatedDateTime { get; set; }
        //public object UpdatedDateTime { get; set; }
        public string FriendlyName { get; set; }
        public string PaymentType { get; set; }
        public PaymentCard PaymentCard { get; set; }
        public object BankAccount { get; set; }
    }

    public class FavouritePaymentMethod : SelectableBase
    {
        public int Id { get; set; }
        //public long CreatedDateTime { get; set; }
        //public long UpdatedDateTime { get; set; }
        public string FriendlyName { get; set; }
        public string PaymentType { get; set; }
        public PaymentCard PaymentCard { get; set; }
        public object BankAccount { get; set; }
    }
    
    #endregion
}
