﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.Models
{
    public class WebResponseArgs : EventArgs
    {
        public string Response { get; private set; }

        //true if there was an exception
        //public bool Error { get; private set; }
        public string Url { get; private set; }

        public WebResponseArgs(string response, string url = "")
        {
            Response = response;
            Url = url;
        }
    }
}
