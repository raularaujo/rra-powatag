﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace PowaTag
{
    /// <summary>
    /// Exposes common navigation commands without requiring the page to inherit from PhoneApplicationPage. Therefore ViewModels can use this to navigate to another page.
    /// </summary>
    public interface INavigationService
    {
        /// <summary>
        /// Event is fired when the NavigationService is Navigating
        /// </summary>
        event NavigatingCancelEventHandler Navigating;

        /// <summary>
        /// URI of the page to navigate to
        /// </summary>
        /// <param name="pageUri"></param>
        void NavigateTo(Uri pageUri);

        /// <summary>
        /// Go to the previous page in the NavigationStack
        /// </summary>
        void GoBack();

        /// <summary>
        /// Go to the next page in the NavigationStack
        /// </summary>
        void GoForward();
    }
}
