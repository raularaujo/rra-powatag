﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace PowaTag.Controls
{
    public partial class ProcessingPurchaseControl : UserControl
    {
        /// <summary>
        /// Dependency property to Bind the Text Value of the Process Message TextBlock of the ProcessingPurchaseScreenControl UserControl.
        /// </summary>
        public static readonly DependencyProperty BindingTextValueProperty = DependencyProperty.Register(
                                          "BindingTextValue",
                                          typeof(string),
                                          typeof(ProcessingPurchaseControl),
                                          new PropertyMetadata(""));

        /// <summary>
        /// Dependency property to Bind the ProgressBar Value of the Progress bar of the ProcessingPurchaseScreenControl UserControl.
        /// </summary>
        public static readonly DependencyProperty BindingProgressBarValueProperty = DependencyProperty.Register(
                                          "BindingProgressBarValue",
                                          typeof(double),
                                          typeof(ProcessingPurchaseControl),
                                          new PropertyMetadata(0.0));
        public string BindingTextValue
        {
            get
            {
                return GetValue(BindingTextValueProperty) as string;
            }
            set
            {
                SetValue(BindingTextValueProperty, value);
            }
        }

        public double BindingProgressBarValue
        {
            get
            {
                var val = GetValue(BindingProgressBarValueProperty);
                return (double)val;
            }
            set
            {
                SetValue(BindingProgressBarValueProperty, value);
            }
        }

        public ProcessingPurchaseControl()
        {
            InitializeComponent();
            LayoutControl.DataContext = this;
        }
    }
}
