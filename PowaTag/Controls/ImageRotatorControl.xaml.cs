﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Collections.ObjectModel;

namespace PowaTag.Controls
{
    public partial class ImageRotatorControl : UserControl
    {
        protected PowaNavigationService powaNavigationService = new PowaNavigationService();
        private List<string> Images = new List<string>();
        private int _currentImage = 0;
        private bool _animate = false;

        //Storyboard liveTileAnimTop = null;
        //Storyboard liveTileAnimBottom = null;

        public ObservableCollection<string> Source
        {
            get { return (ObservableCollection<string>)GetValue(SourceProperty); }
            set
            {
                SetValue(SourceProperty, value);
                if (Source.Count() <= 0) return;
                foreach (var item in Source)
                {
                    AddImage(item);
                }
                _currentImage = 0;
                FadeInImage(_currentImage);
                grid2.Children[0].Opacity = 1;
                if (grid2.Children.Count > 0)
                {
                    ((Storyboard)FindName("liveTileAnimTop")).Begin();
                    _animate = true;
                }
            }
        }

        // Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IEnumerable<string>), typeof(ImageRotatorControl), new PropertyMetadata(null));

        public ImageRotatorControl()
        {
            InitializeComponent();
            //SetUpStoryboard_Top();
            //SetUpStoryboard_Bottom();
        }

        //TODO: Fix codebehind storyboards, they're going straight to the completed event
        ///// <summary>
        ///// Storyboard that handles slide animation Top>Bottom
        ///// </summary>
        //private void SetUpStoryboard_Top()
        //{
        //    liveTileAnimTop = new Storyboard();
        //    liveTileAnimTop.Completed += liveTileAnimTop_Completed_1;

        //    var animTop = new DoubleAnimationUsingKeyFrames();
        //    animTop.BeginTime = new TimeSpan(0, 0, 1);
        //    SplineDoubleKeyFrame key = new SplineDoubleKeyFrame();

        //    key.Value = -50;
        //    key.KeyTime = KeyTime.FromTimeSpan(new TimeSpan(0, 0, 6)); ;

        //    Storyboard.SetTarget(animTop, panel1);
        //    Storyboard.SetTargetName(animTop, "panel1");
        //    Storyboard.SetTargetProperty(animTop, new PropertyPath(TranslateTransform.YProperty));

        //    liveTileAnimTop.Children.Add(animTop);
        //    ImageRotatorRoot.Resources.Add("liveTileAnimTop", liveTileAnimTop);
        //}

        ///// <summary>
        ///// Storyboard that handles slide animation Bottom>Top
        ///// </summary>
        //private void SetUpStoryboard_Bottom()
        //{

        //    liveTileAnimBottom = new Storyboard();
        //    liveTileAnimBottom.Completed += liveTileAnimBottom_Completed_1;

        //    var animBottom = new DoubleAnimationUsingKeyFrames();
        //    animBottom.BeginTime = new TimeSpan(0, 0, 1);
        //    SplineDoubleKeyFrame key = new SplineDoubleKeyFrame();

        //    key.Value = 0;
        //    key.KeyTime = KeyTime.FromTimeSpan(new TimeSpan(0, 0, 6)); ;

        //    Storyboard.SetTarget(animBottom, panel1);
        //    Storyboard.SetTargetName(animBottom, "panel1");
        //    Storyboard.SetTargetProperty(animBottom, new PropertyPath(TranslateTransform.YProperty));

        //    liveTileAnimBottom.Children.Add(animBottom);
        //    ImageRotatorRoot.Resources.Add("liveTileAnimBottom", liveTileAnimBottom);
        //}
        /// <summary>
        /// Adds the given image to the grid
        /// </summary>
        /// <param name="source">Relative path to image</param>
        public void AddImage(string source)
        {
            if (!Images.Contains(source))
            {
                Images.Add(source);
                grid2.Children.Add(new Image()
                {
                    Height = 250,
                    Source = new BitmapImage(new Uri(source, UriKind.Absolute)),
                    HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                    Stretch = System.Windows.Media.Stretch.UniformToFill,
                    Opacity = 0,
                });
            }
        }

        /// <summary>
        /// Calculates the index for the nest image
        /// </summary>
        /// <returns>Grid.Children Index for the next Image</returns>
        private int NextImage()
        {
            return (_currentImage + 1) < grid2.Children.Count ? (_currentImage + 1) : 0;
        }
        #region Animation
        /// <summary>
        /// Fades current image to the next one and begins the next drag (bottom-top) animation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void liveTileAnimTop_Completed_1(object sender, EventArgs e)
        {
            //Storyboard anim = (Storyboard)FindName("liveTileAnimBottom");
            if (!_animate)
            {
                //liveTileAnimBottom.Stop();
                var t = panel1 as TranslateTransform;
                t.Y = 0;
                t.X = 0;
                grid2.RenderTransform = t; return;
            }
            if (_currentImage != NextImage())
            {
                FadeInImage(NextImage());
                FadeOutImage(_currentImage);
            }

            _currentImage = NextImage();

            ((Storyboard)FindName("liveTileAnimBottom")).Begin();
        }

        /// <summary>
        /// Fades current image to the next one and begins the next drag (top-bottom) animation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void liveTileAnimBottom_Completed_1(object sender, EventArgs e)
        {
            //Storyboard anim = (Storyboard)FindName("liveTileAnimTop");
            if (!_animate)
            {
                //liveTileAnimTop.Stop();
                var t = panel1 as TranslateTransform;
                t.Y = 0;
                t.X = 0;
                grid2.RenderTransform = t; return;
            }
            if (_currentImage != NextImage())
            {
                FadeInImage(NextImage());
                FadeOutImage(_currentImage);
            }

            _currentImage = NextImage();

            ((Storyboard)FindName("liveTileAnimTop")).Begin();
        }

        /// <summary>
        /// Fades out image in given index
        /// </summary>
        private void FadeOutImage(int index)
        {
            if (grid2.Children == null || grid2.Children.Count <= 0) return;

            DoubleAnimation fadeOut = new DoubleAnimation { From = 1, To = 0, Duration = TimeSpan.FromSeconds(1) };
            Storyboard.SetTarget(fadeOut, grid2.Children[index]);
            Storyboard.SetTargetProperty(fadeOut, new PropertyPath(Image.OpacityProperty));

            var sb = new Storyboard();
            sb.Children.Add(fadeOut);
            sb.Begin();
        }

        /// <summary>
        /// Fades in image in given index
        /// </summary>
        private void FadeInImage(int index)
        {
            if (grid2.Children == null || grid2.Children.Count <= 0) return;
            DoubleAnimation fadeIn = new DoubleAnimation { From = 0, To = 1, Duration = TimeSpan.FromSeconds(1) };
            Storyboard.SetTarget(fadeIn, grid2.Children[index]);
            Storyboard.SetTargetProperty(fadeIn, new PropertyPath(Image.OpacityProperty));

            var sb = new Storyboard();
            sb.Children.Add(fadeIn);
            sb.Begin();
        }
        #endregion
        /// <summary>
        /// Toggles animation on and off
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grid2_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string urls = String.Join("%", Images);
            
            powaNavigationService.NavigateTo(new Uri("/Views/ProductImageView.xaml?imageurl=" + urls, UriKind.Relative));
            //_animate = !_animate;

            //if (_animate)
            //{
            //    _contentPanel.Height = 200;
            //    ((Storyboard)FindName("liveTileAnimTop")).Begin();
            //}
            //else
            //{
            //    _contentPanel.Height = 250;
            //}
        }
    }
}
