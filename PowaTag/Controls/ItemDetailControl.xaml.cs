﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PowaTag.ViewModels;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PowaTag.Controls
{
    public partial class ItemDetailControl : UserControl, INotifyPropertyChanged
    {
        /// <summary>
        /// Product detail dependency property so you can bind an 
        /// ItemDetailViewModel straight from the XAML.
        /// </summary>
        public ItemDetailViewModel ProductDetail
        {
            get { return (ItemDetailViewModel)GetValue(ProductDetailProperty); }
            set { SetValue(ProductDetailProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ProductDetail.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ProductDetailProperty =
            DependencyProperty.RegisterAttached("ProductDetail", typeof(ItemDetailViewModel), typeof(ItemDetailControl), new PropertyMetadata(null));
        
        /// <summary>
        /// Parent ScrollViewer dependency property so you can bind the
        /// parent scrollviewer straight from the XAML.
        /// </summary>
        public ScrollViewer ParentScrollViewer
        {
            get { return (ScrollViewer)GetValue(ParentScrollViewerProperty); }
            set { SetValue(ParentScrollViewerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ProductDetail.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ParentScrollViewerProperty =
            DependencyProperty.RegisterAttached("ParentScrollViewer", typeof(ScrollViewer), typeof(ScrollViewer), new PropertyMetadata(null));

        ///// <summary>
        ///// Handles the DependencyProperty's changed event.
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private static void OnViewModelChangedCallback(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        //{
        //    //ItemDetailControl c = sender as ItemDetailControl;
        //    //if (c != null)
        //    //{
        //    //    c.OnViewModelChanged();
        //    //}
        //}

        ///// <summary>
        ///// Handles the DependencyProperty's changed event.
        ///// Sets the DataContext/
        ///// </summary>
        //protected virtual void OnViewModelChanged()
        //{
        //    //DataContext = ProductDetail;

        //    //NotifyPropertyChanged("ProductDetail");
        //}

        public ItemDetailControl()
        {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([CallerMemberName]String propertyName = "")
        {
            var propHandler = PropertyChanged;
            if (propHandler != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void lpOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((DataContext as ItemDetailViewModel) != null)
                (DataContext as ItemDetailViewModel).ListPicker_SelectionChanged(sender, e);
        }

        private void lpOptions_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var pos = Application.Current.RootVisual.TransformToVisual(lpOptions).Transform(new Point(0, 0));
            if (ParentScrollViewer != null && Math.Abs(pos.Y) + (e.NewSize.Height - e.PreviousSize.Height) > ParentScrollViewer.ViewportHeight)
            {
                ParentScrollViewer.ScrollToVerticalOffset(ParentScrollViewer.VerticalOffset + (e.NewSize.Height - e.PreviousSize.Height));
            }
        }
    }
}
