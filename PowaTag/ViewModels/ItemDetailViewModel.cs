﻿using Microsoft.Phone.Controls;
using PowaTag.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace PowaTag.ViewModels
{
    public class ItemDetailViewModel : ViewModelBase
    {
        #region Properties

        private SelectableBase _productDetail;
        /// <summary>
        /// Product Detail (attribute or option)
        /// </summary>
        public SelectableBase ProductDetail
        {
            get { return _productDetail; }
            set { _productDetail = value; NotifyPropertyChanged("DetailName"); }
        }

        /// <summary>
        /// Item Detail element Name
        /// </summary>
        public string DetailName
        {
            get { return ProductDetail != null ? toTitleCase(ProductDetail.Name) : "Couldn't get DetailName"; }
        }

        /// <summary>
        /// Item Detail element Option (for ProductAttribute)
        /// </summary>
        public string DetailOption
        {
            get
            {
                var detail = ProductDetail as ProductAttribute;
                return detail != null ? detail.Value : "";
            }
        }

        /// <summary>
        /// Item Detail element Options (for ProductOptions
        /// </summary>
        public IEnumerable<string> DetailOptions
        {
            get
            {
                if (ProductDetail is ProductOption)
                    if (((ProductOption)ProductDetail).ProductOptionValues != null)
                        return ((ProductOption)ProductDetail).ProductOptionValues.OrderBy(o => o.SortPos).Select(v => v.Name);
                return new List<string>();
            }
        }

        /// <summary>
        /// Controls the visibility of the ProductAttribute TextBlock
        /// </summary>
        public Visibility AttributeVisibility
        {
            get { return !String.IsNullOrEmpty(DetailOption) ? Visibility.Visible : Visibility.Collapsed; }
        }

        /// <summary>
        /// Controls the visibility of the ProductOptions ListPicker
        /// </summary>
        public Visibility OptionVisibility
        {
            get { return DetailOptions.Count() > 0 ? Visibility.Visible : Visibility.Collapsed; }
        }

        private string _selectedItem;
        /// <summary>
        /// Get or Set the Selected Option
        /// </summary>
        public string SelectedItem
        {
            get { return _selectedItem; }
        }

        #endregion

        /// <summary>
        /// Constructor. Orders the product options and bulds the appropriate 
        /// control.
        /// </summary>
        /// <param name="productDetail">Product Attribute or Option</param>
        /// <param name="sv">Refernce to the ScrollViewer</param>
        public ItemDetailViewModel(SelectableBase productDetail)
        {
            ProductDetail = productDetail;

            if (ProductDetail is ProductOption)
            {
                _selectedItem = DetailOptions.ElementAt(0);
                ProductOptionValue pov = (ProductDetail as ProductOption).ProductOptionValues.FirstOrDefault(o => o.Name.Equals(_selectedItem));
                App.OrderModel.Order.Products.First().ProductOptions.FirstOrDefault(o => o.Name.Equals(ProductDetail.Name)).SelectedProductOptionValue = pov;
            }
            else
            { _selectedItem = DetailOption; }
        }

        /// <summary>
        /// Handles selection changed events so we can get the SelectedItem 
        /// regardless of the control in use;
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListPicker lp = sender as ListPicker;
            if (lp != null)
            {
                if (lp.SelectedIndex >= 0 && lp.SelectedIndex < DetailOptions.Count())
                {
                    _selectedItem = DetailOptions.ElementAt(lp.SelectedIndex);

                    ProductOptionValue pov = (ProductDetail as ProductOption).ProductOptionValues.FirstOrDefault(o => o.Name.Equals(_selectedItem));
                    App.OrderModel.Order.Products.First().ProductOptions.FirstOrDefault(o => o.Name.Equals(ProductDetail.Name)).SelectedProductOptionValue = pov;
                }
            }
        }

        /// <summary>
        /// Converts to title case: each word starts with an upper case.
        /// </summary>
        /// <remarks>using System.Text;</remarks>
        private string toTitleCase(string value)
        {
            if (value == null)
                return null;
            if (value.Length == 0)
                return value;

            StringBuilder result = new StringBuilder(value);
            result[0] = char.ToUpper(result[0]);
            for (int i = 1; i < result.Length; ++i)
            {
                if (char.IsWhiteSpace(result[i - 1]))
                    result[i] = char.ToUpper(result[i]);
                else
                    result[i] = char.ToLower(result[i]);
            }
            return result.ToString();
        }
    }
}
