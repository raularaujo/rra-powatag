﻿using PowaTag.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag
{
    class OrderDataModel : ViewModelBase
    {
        private ObservableCollection<string> _imageList = new ObservableCollection<string>();
        /// <summary>
        /// List of Image URLs for the ImageRotatorControl
        /// </summary>
        public ObservableCollection<string> ImageList
        {
            get { return _imageList; }
            private set { _imageList = value; NotifyPropertyChanged(); }
        }

        private string _applicationId;
        public string ApplicationIdValue
        {
            get { return _applicationId; }
            set
            {
                _applicationId = value;
                NotifyPropertyChanged(ApplicationIdValue);
            }
        }

        private string _productImage;
        public string ProductImage
        {
            get { return _productImage; }
            set
            {
                _productImage = value;
                NotifyPropertyChanged(ProductImage);
            }
        }

        private string _creditCardValue;
        public string CreditCardValue
        {
            get { return _creditCardValue; }
            set
            {
                _creditCardValue = value;
                NotifyPropertyChanged(CreditCardValue);
            }
        }

        private string _deliveryAddress;
        public string DeliveryAddress
        {
            get { return _deliveryAddress; }
            set 
            { 
                _deliveryAddress = value;
                NotifyPropertyChanged(DeliveryAddress);
            }
        }

        private string _latitude;
        public string Latitude
        {
            get { return _latitude; }
            set 
            { 
                _latitude = value;
                NotifyPropertyChanged(Latitude);
            }
        }

        private string _longitude;
        public string Longitude
        {
            get { return _longitude; }
            set 
            { 
                _longitude = value;
                NotifyPropertyChanged(Longitude);
            }
        }

        private string _merchantValue;
        public string MerchantValue
        {
            get { return _merchantValue; }
            set
            {
                _merchantValue = value;
                NotifyPropertyChanged(MerchantValue);
            }
        }

        private string _orderName;
        public string OrderName
        {
            get { return _orderName; }
            set
            {
                _orderName = value;
                NotifyPropertyChanged(OrderName);
            }
        }

        private string _panSequenceValue;
        public string PanSequenceValue
        {
            get { return _panSequenceValue; }
            set
            {
                _panSequenceValue = value;
                NotifyPropertyChanged(PanSequenceValue);
            }
        }

        private string _paymentMethodValue;
        public string PaymentMethodValue
        {
            get { return _paymentMethodValue; }
            set
            {
                _paymentMethodValue = value;
                NotifyPropertyChanged(PaymentMethodValue);
            }
        }

        private string _priceValue;
        public string PriceValue
        {
            get { return _priceValue; }
            set 
            { 
                _priceValue = value;
                NotifyPropertyChanged(PriceValue);
            }
        }

        private string _productDescription;
        public string ProductDescription
        {
            get { return _productDescription; }
            set
            {
                _productDescription = value;
                NotifyPropertyChanged(ProductDescription);
            }
        }

        private string _productName;
        public string ProductName
        {
            get { return _productName; }
            set
            {
                _productName = value;
                NotifyPropertyChanged(ProductName);
            }
        }

        private string _receipt;
        public string ReceiptValue
        {
            get { return _receipt; }
            set
            {
                _receipt = value;
                NotifyPropertyChanged(ReceiptValue);
            }
        }

        private string _terminalIdValue;
        public string TerminalIdValue
        {
            get { return _terminalIdValue; }
            set
            {
                _terminalIdValue = value;
                NotifyPropertyChanged(TerminalIdValue);
            }
        }
             
    }
}
