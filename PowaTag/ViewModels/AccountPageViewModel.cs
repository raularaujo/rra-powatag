﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.ViewModels
{
    class AccountPageViewModel : ViewModelBase
    {
        
        private string _emailAddressValue;
        public string EmailAddressValue
        {
            get { return _emailAddressValue; }
            set
            {
                _emailAddressValue = value;
                NotifyPropertyChanged("EmailAddressValue");
            }
        }

        private string _usernameValue;
        public string UsernameValue
        {
            get { return _usernameValue; }
            set
            {
                _usernameValue = value;
                NotifyPropertyChanged("UsernameValue");
            }
        }

        private string _billingNameValue;
        public string BillingNameValue
        {
            get { return _billingNameValue; }
            set
            {
                _billingNameValue = value;
                NotifyPropertyChanged("BillingNameValue");
            }
        }

        private string _billingAddressValue;
        public string BillingAddressValue
        {
            get { return _billingAddressValue; }
            set
            {
                _billingAddressValue = value;
                NotifyPropertyChanged("BillingAddressValue");
            }
        }

        private string _billingTownCityValue;
        public string BillingTownCityValue
        {
            get { return _billingTownCityValue; }
            set
            {
                _billingTownCityValue = value;
                NotifyPropertyChanged("BillingTownCityValue");
            }
        }

        private string _billingCountyValue;
        public string BillingCountyValue
        {
            get { return _billingCountyValue; }
            set
            {
                _billingCountyValue = value;
                NotifyPropertyChanged("BillingCountyValue");
            }
        }

        private string _billingPostcodeValue;
        public string BillingPostcodeValue
        {
            get { return _billingPostcodeValue; }
            set
            {
                _billingPostcodeValue = value;
                NotifyPropertyChanged("BillingPostcodeValue");
            }
        }

        private string _billingCountryValue;
        public string BillingCountryValue
        {
            get { return _billingCountryValue; }
            set
            {
                _billingCountryValue = value;
                NotifyPropertyChanged("BillingCountryValue");
            }
        }

        private string _deliveryNameValue;
        public string DeliveryNameValue
        {
            get { return _deliveryNameValue; }
            set
            {
                _deliveryNameValue = value;
                NotifyPropertyChanged("DeliveryNameValue");
            }
        }

        private string _deliveryAddressValue;
        public string DeliveryAddressValue
        {
            get { return _deliveryAddressValue; }
            set
            {
                _deliveryAddressValue = value;
                NotifyPropertyChanged("DeliveryAddressValue");
            }
        }

        private string _deliveryTownCityValue;
        public string DeliveryTownCityValue
        {
            get { return _deliveryTownCityValue; }
            set
            {
                _deliveryTownCityValue = value;
                NotifyPropertyChanged("DeliveryTownCityValue");
            }
        }

        private string _deliveryCountyValue;
        public string DeliveryCountyValue
        {
            get { return _deliveryCountyValue; }
            set
            {
                _deliveryCountyValue = value;
                NotifyPropertyChanged("DeliveryCountyValue");
            }
        }

        private string _deliveryPostcodeValue;
        public string DeliveryPostcodeValue
        {
            get { return _deliveryPostcodeValue; }
            set
            {
                _deliveryPostcodeValue = value;
                NotifyPropertyChanged("DeliveryPostcodeValue");
            }
        }

        private string _deliveryCountryValue;
        public string DeliveryCountryValue
        {
            get { return _deliveryCountryValue; }
            set
            {
                _deliveryCountryValue = value;
                NotifyPropertyChanged("DeliveryCountryValue");
            }
        }

        private string _cardTypeValue;
        public string CardTypeValue
        {
            get { return _cardTypeValue; }
            set
            {
                _cardTypeValue = value;
                NotifyPropertyChanged("CardTypeValue");
            }
        }

        private string _cardNumberValue;
        public string CardNumberValue
        {
            get { return _cardNumberValue; }
            set
            {
                _cardNumberValue = value;
                NotifyPropertyChanged("CardNumberValue");
            }
        }

        private string _cardExpiryDateValue;
        public string CardExpiryDateValue
        {
            get { return _cardExpiryDateValue; }
            set
            {
                _cardExpiryDateValue = value;
                NotifyPropertyChanged("CardExpiryDateValue");
            }
        }
    }
}
