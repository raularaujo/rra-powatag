﻿using PowaTag.Models;
using PowaTag.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace PowaTag.ViewModels
{
    public class CvvVerificationViewModel : ViewModelBase
    {
        //public ICommand SavePINCommand { get; set; }
        public ICommand EnterCommand { get; set; }
        private RequestClientManager rcm;
        public Dispatcher UIDispatcher { get; set; }

        public string addressId = "", paymentId = "";
        string latitude = "0", longitude = "0";

        private string _pin;

        public string Pin
        {
            get { return _pin; }
            set
            {
                _pin = value;
                if (_pin.Length == 3)
                { EnterCommand.CanExecute(null); Verify(null); }
            }
        }


        public CvvVerificationViewModel()
        {
            //SavePINCommand = new DelegateCommand(SavePIN, CanSavePIN);
            EnterCommand = new DelegateCommand(Verify, PinValid);
            rcm = new RequestClientManager("application/json;charset=UTF-8", "gzip", Utilities.API_KEY, "application/json; charset=UTF-8", Utilities.DEVICE_FINGERPRINT, "POST");
            rcm.PurchaseCompleteCompleted += rcm_PurchaseCompleteCompleted;
            GetCurrentLocation();
        }

        private async void GetCurrentLocation()
        {
            string location = await PowaTag.Geolocation.TagGeolocator.GetCurrentLocation();
            latitude = location.Split(':')[0];
            longitude = location.Split(':')[1];
        }

        void rcm_PurchaseCompleteCompleted(object sender, WebResponseArgs e)
        {
            if (!e.Response.StartsWith("ERROR"))
            {
                UIDispatcher.BeginInvoke(() =>
                {
                    powaNavigationService.NavigateTo(new Uri("/Views/ReceiptView.xaml", UriKind.Relative));
                });
            }
            else
            {
                OnShowMessageBox(new MessageEventArgs("Sorry, an error occurred :("));
            }
        }


        private string _header = "Verification";
        public string Header
        {
            get { return _header; }
            set
            {
                if (_header != value)
                {
                    _header = value;
                    NotifyPropertyChanged("Header");
                }
            }
        }


        private void Verify(object param)
        {

            ProgressVisibility = System.Windows.Visibility.Visible;
            if (Pin.Length == 3)
            {

                //Used to transmit the options details 
                Dictionary<string, Dictionary<string, Option>> optionsDict = new Dictionary<string, Dictionary<string, Option>>();
                Dictionary<string, Option> optDict = new Dictionary<string, Option>();

                foreach (var op in App.OrderModel.Order.Products.First().ProductOptions)
                {
                    var selected = op.SelectedProductOptionValue as ProductOptionValue;
                    optDict.Add(op.Name, new Option(selected.Code, selected.Name, op.MappedId as string));
                }

                if (optDict.Count > 0)
                    optionsDict.Add(App.OrderModel.Order.Products.First().Code, optDict); //use to pass options
                else
                    optionsDict.Add(App.OrderModel.Order.Products.First().Code, null); //use to pass no options

                int index = App.OrderModel.Order.User.FavouriteDeliveryAddress.Id;
                App.OrderModel.Order.DeliveryAddress = App.OrderModel.Order.User.Addresses.ElementAt(index >= App.OrderModel.Order.User.Addresses.Count() ? index - 1 : index).Name;

                index = App.OrderModel.Order.User.FavouritePaymentMethod.Id;
                App.OrderModel.Order.PaymentMethod = App.OrderModel.Order.User.PaymentMethods.ElementAt(index >= App.OrderModel.Order.User.PaymentMethods.Count() ? index - 1 : index).FriendlyName;

                if (Utilities.IsNetworkAvailable)
                    rcm.SendPurchaseRequest(RequestClientManager.PurchaseEndpoints.PurchaseComplete, Utilities.REFERENCE_ID, Utilities.GetHMAC(Utilities.REFERENCE_ID, Utilities.SECRET_KEY), latitude, longitude, Pin, addressId, paymentId, optionsDict);
                else
                    OnShowMessageBox(new MessageEventArgs(Resources.AppResources.NetworkErrorMessage));
            }
            else
            {
                OnShowMessageBox(new MessageEventArgs("PIN rejected"));
            }

        }

        private System.Windows.Visibility _progressVisibility = System.Windows.Visibility.Collapsed;
        /// <summary>
        /// Toggles the visibility of the loading screen
        /// </summary>
        public System.Windows.Visibility ProgressVisibility
        {
            get { return _progressVisibility; }
            set
            {
                _progressVisibility = value;
                NotifyPropertyChanged();
            }
        }

        ///// <summary>
        ///// Save the PIN number to file
        ///// </summary>
        ///// <param name="param">Pin to save (as string)</param>
        //private void SavePIN(object param)
        //{
        //    string finalPin = PinFirstChar + PinSecondChar + PinThirdChar;

        //    //Make sure at least 3 characters have been entered
        //    if (finalPin.Length != 3)
        //    {      
        //        OnShowMessageBox(new MessageEventArgs("Please enter three digits"));
        //        return;
        //    }

        //    //Ensure that all characters are digits
        //    foreach (char c in finalPin)
        //    {
        //        if (!char.IsDigit(c))
        //        {
        //            OnShowMessageBox(new MessageEventArgs("Please enter three digits"));                 
        //            return;
        //        }
        //    }

        //    if (!PINUtilities.DoesPINExist())
        //    {
        //        PINUtilities.SavePIN(finalPin);
        //    }
        //    else
        //    {
        //        OnShowMessageBox(new MessageEventArgs("A PIN has already been set"));
        //    }
        //}

        //private bool CanSavePIN(object param)
        //{
        //    //string combinedPIN = PinFirstChar + PinSecondChar + PinThirdChar;
        //    ////only enable if all three textboxes are filled with one character
        //    //return combinedPIN.Length == 3;
        //}        //private bool CanSavePIN(object param)
        private bool PinValid(object param)
        {
            //string combinedPIN = PinFirstChar + PinSecondChar + PinThirdChar;
            ////only enable if all three textboxes are filled with one character
            if (String.IsNullOrEmpty(Pin)) return false;
            else
                return Pin.Length == 3;
        }

        #region Character Bindings

        //private string _pinFirstChar = "";
        //public string PinFirstChar
        //{
        //    get { return _pinFirstChar; }
        //    set
        //    {
        //        if (_pinFirstChar != value)
        //        {
        //            _pinFirstChar = value;
        //            NotifyPropertyChanged("PinFirstChar");
        //            //raise the CanExecute check so that we can see if the Save PIN button should be allowed
        //            SavePINCommand.CanExecute(null);
        //            EnterCommand.CanExecute(null);
        //        }
        //    }
        //}


        //private string _pinSecondChar = "";
        //public string PinSecondChar
        //{
        //    get { return _pinSecondChar; }
        //    set
        //    {
        //        if (_pinSecondChar != value)
        //        {
        //            _pinSecondChar = value;
        //            NotifyPropertyChanged("PinSecondChar");
        //            SavePINCommand.CanExecute(null);
        //            EnterCommand.CanExecute(null);
        //        }
        //    }
        //}


        //private string _pinThirdChar = "";
        //public string PinThirdChar
        //{
        //    get { return _pinThirdChar; }
        //    set
        //    {
        //        if (_pinThirdChar != value)
        //        {
        //            _pinThirdChar = value;
        //            NotifyPropertyChanged("PinThirdChar");
        //            SavePINCommand.CanExecute(null);
        //            EnterCommand.CanExecute(null);
        //        }
        //    }
        //}
        #endregion
    }
}
