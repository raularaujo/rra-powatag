﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.ViewModels
{
    class ProductImageViewModel : ViewModelBase
    {
        //private string _productImageURL;
        //public string ProductImageURL
        //{
        //    get { return _productImageURL; }
        //    set { _productImageURL = value; NotifyPropertyChanged(); }
        //}

        private ObservableCollection<string> _productImageURL = new ObservableCollection<string>();
        public ObservableCollection<string> ProductImageURL
        {
            get { return _productImageURL; }
            set { _productImageURL = value; NotifyPropertyChanged(); }
        }

        public ProductImageViewModel()
        {
        }

        public void AddImages(string[] images)
        {
            foreach (var item in images)
            {
                AddImage(item);
            }
        }

        public void AddImage(string image)
        {
            ProductImageURL.Add(image);
        }
    }
}
