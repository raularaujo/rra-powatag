﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.ViewModels
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        //Use this to navigate rather than NavigationService as it's available anywhere in any ViewModel
        protected PowaNavigationService powaNavigationService = new PowaNavigationService();


        protected void NotifyPropertyChanged([CallerMemberName]String propertyName = "")
        {  
            var propHandler = PropertyChanged;
            if (propHandler != null)
               PropertyChanged(this, new PropertyChangedEventArgs(propertyName));            
        }


        public event EventHandler<MessageEventArgs> ShowMessageBox;

        protected virtual void OnShowMessageBox(MessageEventArgs e)
        {
            var handler = ShowMessageBox;
            if (handler != null)
            {
                handler(this, e);
            }
        }

    }
}
