﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PowaTag.ViewModels
{
    /// <summary>
    /// Data Model for each History Item in the History view.
    /// </summary>
    class HistoryPageViewModel : ViewModelBase
    {
     
        
        private string _partialDate;
        public string PartialDate
        {
            get { return _partialDate; }
            set
            {
                _partialDate = value;
                NotifyPropertyChanged("PartialDate");
            }
        }

        private string _sourceProductImage;
        public string SourceProductImage
        {
            get { return _sourceProductImage; }
            set
            {
                _sourceProductImage = value;
                NotifyPropertyChanged("SourceProductImage");
            }
        }

        private string _productName;
        public string ProductName
        {
            get { return _productName; }
            set
            {
                _productName = value;
                NotifyPropertyChanged("ProductName");
            }
        }

        private string _purchaseDate;
        public string PurchaseDate
        {
            get { return _purchaseDate; }
            set
            {
                _purchaseDate = value;
                NotifyPropertyChanged("PurchaseDate");
            }
        }

        private string _productPrice;
        public string ProductPrice
        {
            get { return _productPrice; }
            set
            {
                _productPrice = value;
                NotifyPropertyChanged("ProductPrice");
            }
        }

    }
}
