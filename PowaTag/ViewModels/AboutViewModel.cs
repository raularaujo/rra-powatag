﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.ViewModels
{
    public class AboutViewModel : ViewModelBase
    {
        private string _header = "About";
        public string Header
        {
            get { return _header; }
            set
            {
                if (_header != value)
                {
                    _header = value;
                    NotifyPropertyChanged("Header");
                }
            }
        }

        private string _version;
        public string Version
        {
            get { return _version; }
            set
            {
                if (_version != value)
                {
                    _version = value;
                    NotifyPropertyChanged("Version");
                }
            }
        }

        private string _build;
        public string Build
        {
            get { return _build; }
            set
            {
                if (_build != value)
                {
                    _build = value;
                    NotifyPropertyChanged("Build");
                }
            }
        }

        private string _date;
        public string Date
        {
            get { return _date; }
            set
            {
                if (_date != value)
                {
                    _date = value;
                    NotifyPropertyChanged("Date");
                }
            }
        }

        private string _web;
        public string Web
        {
            get { return _web; }
            set
            {
                if (_web != value)
                {
                    _web = value;
                    NotifyPropertyChanged("Web");
                }
            }
        }

        private string _facebook;
        public string Facebook
        {
            get { return _facebook; }
            set
            {
                if (_facebook != value)
                {
                    _facebook = value;
                    NotifyPropertyChanged("Facebook");
                }
            }
        }

        private string _twitter;
        public string Twitter
        {
            get { return _twitter; }
            set
            {
                if (_twitter != value)
                {
                    _twitter = value;
                    NotifyPropertyChanged("Twitter");
                }
            }
        }
         
    }
}
