﻿using PowaTag.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PowaTag.ViewModels
{
    public class SignUpViewModel : ViewModelBase
    {
        #region Properties

        private RequestClientManager _requestManager;

        #region User Details

        private string _username;

        public string Username
        {
            get { return _username; }
            set { _username = value; NotifyPropertyChanged(); }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set { _password = value; NotifyPropertyChanged(); }
        }

        private string _passwordCheck;

        public string PasswordCheck
        {
            get { return _passwordCheck; }
            set { _passwordCheck = value; NotifyPropertyChanged(); }
        }

        private string _gender;

        public string Gender
        {
            get { return _gender; }
            set { _gender = value; NotifyPropertyChanged(); }
        }

        private DateTime _dateOfBirth;

        public DateTime DateOfBirth
        {
            get { return _dateOfBirth; }
            set { _dateOfBirth = value; NotifyPropertyChanged(); }
        }

        private string _emailAddress;

        public string EmailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = value; NotifyPropertyChanged(); }
        }

        #endregion

        #region Delivery Address
        private string _deliveryAddressName;

        public string DeliveryAddressName
        {
            get { return _deliveryAddressName; }
            set { _deliveryAddressName = value; NotifyPropertyChanged(); }
        }

        private string _deliveryAddressLine1;

        public string DeliveryAddressLine1
        {
            get { return _deliveryAddressLine1; }
            set { _deliveryAddressLine1 = value; NotifyPropertyChanged(); }
        }

        private string _deliveryAddressLine2;

        public string DeliveryAddressLine2
        {
            get { return _deliveryAddressLine2; }
            set { _deliveryAddressLine2 = value; NotifyPropertyChanged(); }
        }

        private string _deliveryAddressLine3;

        public string DeliveryAddressLine3
        {
            get { return _deliveryAddressLine3; }
            set { _deliveryAddressLine3 = value; NotifyPropertyChanged(); }
        }

        private string _deliveryAddressLine4;

        public string DeliveryAddressLine4
        {
            get { return _deliveryAddressLine4; }
            set { _deliveryAddressLine4 = value; NotifyPropertyChanged(); }
        }

        private string _deliveryAddressCIty;

        public string DeliveryAddressCity
        {
            get { return _deliveryAddressCIty; }
            set { _deliveryAddressCIty = value; NotifyPropertyChanged(); }
        }

        private string _deliveryAddressCounty;

        public string DeliveryAddressCounty
        {
            get { return _deliveryAddressCounty; }
            set { _deliveryAddressCounty = value; NotifyPropertyChanged(); }
        }

        private string _deliveryAddressPostcode;

        public string DeliveryAddressPostcode
        {
            get { return _deliveryAddressPostcode; }
            set { _deliveryAddressPostcode = value; NotifyPropertyChanged(); }
        }

        private string _deliveryAddressCountry;

        public string DeliveryAddressCountry
        {
            get { return _deliveryAddressCountry; }
            set { _deliveryAddressCountry = value; NotifyPropertyChanged(); }
        }

        #endregion

        #region Payment Method
        private string _cardType;

        public string CardType
        {
            get { return _cardType; }
            set { _cardType = value; NotifyPropertyChanged(); }
        }

        private string _cardNumber;

        public string CardNumber
        {
            get { return _cardNumber; }
            set { _cardNumber = value; NotifyPropertyChanged(); }
        }

        private DateTime _cardExpiryDate;

        public DateTime CardExpiryDate
        {
            get { return _cardExpiryDate; }
            set { _cardExpiryDate = value; NotifyPropertyChanged(); }
        }

        #endregion

        #region Payment Address

        private string _paymentAddressName;

        public string PaymentAddressName
        {
            get { return _paymentAddressName; }
            set { _paymentAddressName = value; NotifyPropertyChanged(); }
        }

        private string _paymentAddressLine1;

        public string PaymentAddressLine1
        {
            get { return _paymentAddressLine1; }
            set { _paymentAddressLine1 = value; NotifyPropertyChanged(); }
        }

        private string _paymentAddressLine2;

        public string PaymentAddressLine2
        {
            get { return _paymentAddressLine2; }
            set { _paymentAddressLine2 = value; NotifyPropertyChanged(); }
        }

        private string _paymentAddressLine3;

        public string PaymentAddressLine3
        {
            get { return _paymentAddressLine3; }
            set { _paymentAddressLine3 = value; NotifyPropertyChanged(); }
        }

        private string _paymentAddressLine4;

        public string PaymentAddressLine4
        {
            get { return _paymentAddressLine4; }
            set { _paymentAddressLine4 = value; NotifyPropertyChanged(); }
        }

        private string _paymentAddressCIty;

        public string PaymentAddressCity
        {
            get { return _paymentAddressCIty; }
            set { _paymentAddressCIty = value; NotifyPropertyChanged(); }
        }

        private string _paymentAddressCounty;

        public string PaymentAddressCounty
        {
            get { return _paymentAddressCounty; }
            set { _paymentAddressCounty = value; NotifyPropertyChanged(); }
        }

        private string _paymentAddressPostcode;

        public string PaymentAddressPostcode
        {
            get { return _paymentAddressPostcode; }
            set { _paymentAddressPostcode = value; NotifyPropertyChanged(); }
        }

        private string _paymentAddressCountry;
        private PowaNavigationService _powaNavigationService;

        public string PaymentAddressCountry
        {
            get { return _paymentAddressCountry; }
            set { _paymentAddressCountry = value; NotifyPropertyChanged(); }
        }

        #endregion

        #endregion

        public SignUpViewModel()
        {
            _powaNavigationService = new PowaNavigationService();

            _dateOfBirth = DateTime.Now;
            _cardExpiryDate = DateTime.Now;
            
            _requestManager = new RequestClientManager("application/json;charset=UTF-8", "gzip", Utilities.API_KEY, "application/json; charset=UTF-8", Utilities.DEVICE_FINGERPRINT, "POST");
            _requestManager.AccountSignupCompleted += _requestManager_AccountSignupCompleted;
            _requestManager.EmailVerificationCompleted += (s, e) =>
            {
                System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    switch (e.Response)
                    {
                        case "OK": // Email exists, ask to log in
                            System.Windows.MessageBox.Show("200 OK!");
                            break;
                        case "BadRequest": // Not a valid email address
                            System.Windows.MessageBox.Show("400 Bad Request!");
                            break;
                        case "NotFound": // Email not registered / free to use
                            System.Windows.MessageBox.Show("404 Not Found!");
                            break;
                    }
                });
            };
        }

        void _requestManager_AccountSignupCompleted(object sender, Models.WebResponseArgs e)
        {
            if (e.Response.Contains("error"))
                System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    MessageBox.Show("Something went wrong");
                });
            else
                _powaNavigationService.NavigateTo(new Uri("/Views/MainPage.xaml"));
        }

        public void RequestSignUp()
        {
            string locale = System.Globalization.CultureInfo.CurrentCulture.Name.Replace("-", "_");

            if (String.IsNullOrEmpty(DeliveryAddressCountry) &&
                String.IsNullOrEmpty(DeliveryAddressCity) &&
                String.IsNullOrEmpty(DeliveryAddressCounty) &&
                String.IsNullOrEmpty(DeliveryAddressName) &&
                String.IsNullOrEmpty(DeliveryAddressLine1) &&
                String.IsNullOrEmpty(DeliveryAddressLine2) &&
                String.IsNullOrEmpty(DeliveryAddressLine3) &&
                String.IsNullOrEmpty(DeliveryAddressLine4) &&
                String.IsNullOrEmpty(DeliveryAddressPostcode) &&
                String.IsNullOrEmpty(PaymentAddressCountry) &&
                String.IsNullOrEmpty(PaymentAddressCity) &&
                String.IsNullOrEmpty(PaymentAddressCounty) &&
                String.IsNullOrEmpty(PaymentAddressName) &&
                String.IsNullOrEmpty(PaymentAddressLine1) &&
                String.IsNullOrEmpty(PaymentAddressLine2) &&
                String.IsNullOrEmpty(PaymentAddressLine3) &&
                String.IsNullOrEmpty(PaymentAddressLine4) &&
                String.IsNullOrEmpty(PaymentAddressPostcode) &&
                String.IsNullOrEmpty(CardType) &&
                String.IsNullOrEmpty(CardNumber) &&
                String.IsNullOrEmpty(EmailAddress) &&
                String.IsNullOrEmpty(Gender) &&
                String.IsNullOrEmpty(Username) &&
                String.IsNullOrEmpty(Password))
            {
                MessageBox.Show("Please fill all fields"); return;
            }


            #region dictionaries
            int[] userDateOfBirthArray = { DateOfBirth.Year, DateOfBirth.Month, DateOfBirth.Day };

            //Country addressCountry = new Country()
            //{
            //    Alpha2Code = deliveryAddressCountryAlpha2Code,
            //    Alpha3Code = deliveryAddressCountryAlpha3Code,
            //    Id = Int32.Parse(deliveryAddressCountryID),
            //    Name = deliveryAddressCountryName,
            //    NumericCode = Int32.Parse(deliveryAddressCountryNumericCode)
            //};
            var addressCountryDictionary = new Dictionary<string, object>();
            addressCountryDictionary.Add("alpha2Code", "GB");
            addressCountryDictionary.Add("alpha3Code", "GBR");
            addressCountryDictionary.Add("id", 232);
            addressCountryDictionary.Add("name", DeliveryAddressCountry);
            addressCountryDictionary.Add("numericCode", 826);

            //Address address = new Address()
            //{
            //    City = deliveryAddressCity,
            //    Country = addressCountry,
            //    County = deliveryAddressCounty,
            //    FriendlyName = deliveryAddressFriendlyName,
            //    Line1 = deliveryAddressLine1,
            //    Line2 = deliveryAddressLine2,
            //    Line3 = deliveryAddressLine3,
            //    Line4 = deliveryAddressLine4,
            //    Name = deliveryAddressName,
            //    PostCode = deliveryAddressPostCode
            //};
            var addressDictionary = new Dictionary<string, object>();
            addressDictionary.Add("city", DeliveryAddressCity);
            addressDictionary.Add("country", /*addressCountry*/addressCountryDictionary);
            addressDictionary.Add("county", DeliveryAddressCounty);
            addressDictionary.Add("friendlyName", DeliveryAddressName);
            addressDictionary.Add("line1", DeliveryAddressLine1);
            addressDictionary.Add("line2", DeliveryAddressLine2);
            addressDictionary.Add("line3", DeliveryAddressLine3);
            addressDictionary.Add("line4", DeliveryAddressLine4);
            addressDictionary.Add("name", DeliveryAddressName);
            addressDictionary.Add("postCode", DeliveryAddressPostcode);

            object[] addressArray = { /*address*/addressDictionary };

            //Country paymentCountry = new Country()
            //{
            //    Alpha2Code = paymentAddressCountryAlpha2Code,
            //    Alpha3Code = paymentAddressCountryAlpha3Code,
            //    Id = paymentAddressCountryID,
            //    Name = paymentAddressCountryName,
            //    NumericCode = paymentAddressCountryNumericCode
            //};
            var paymentCardAddressCountryDictionary = new Dictionary<string, object>();
            paymentCardAddressCountryDictionary.Add("alpha2Code", "GB");
            paymentCardAddressCountryDictionary.Add("alpha3Code", "GBR");
            paymentCardAddressCountryDictionary.Add("id", 232);
            paymentCardAddressCountryDictionary.Add("name", PaymentAddressCountry);
            paymentCardAddressCountryDictionary.Add("numericCode", 826);

            //Address cardAddress = new Address()
            //{
            //    City = paymentAddressCity,
            //    Country = paymentCountry,
            //    County = paymentAddressCounty,
            //    FriendlyName = paymentAddressFriendlyName,
            //    Line1 = paymentAddressLine1,
            //    Line2 = paymentAddressLine2,
            //    Line3 = paymentAddressLine3,
            //    Line4 = paymentAddressLine4,
            //    Name = paymentAddressName,
            //    PostCode = paymentAddressPostCode
            //};
            var paymentCardAddressDictionary = new Dictionary<string, object>();

            paymentCardAddressDictionary.Add("city", PaymentAddressCity);
            paymentCardAddressDictionary.Add("country", /*paymentCountry*/paymentCardAddressCountryDictionary);
            paymentCardAddressDictionary.Add("county", PaymentAddressCounty);
            paymentCardAddressDictionary.Add("friendlyName", PaymentAddressName);
            paymentCardAddressDictionary.Add("line1", PaymentAddressLine1);
            paymentCardAddressDictionary.Add("line2", PaymentAddressLine2);
            paymentCardAddressDictionary.Add("line3", PaymentAddressLine3);
            paymentCardAddressDictionary.Add("line4", PaymentAddressLine4);
            paymentCardAddressDictionary.Add("name", PaymentAddressName);
            paymentCardAddressDictionary.Add("postCode", PaymentAddressPostcode);

            //PaymentCard paymentCard = new PaymentCard()
            //{
            //    Address = cardAddress,
            //    Brand = paymentCardBrand,
            //    ExpiresEnd = paymentCardExpiryDate,
            //    IssueNumber = paymentCardIssueNumber,
            //    FriendlyName = paymentMethodFriendlyName,
            //    NameOnCard = paymentCardNameOnCard,
            //    PrimaryAccountNumber = Int64.Parse(paymentCardPAN),
            //    ShortPrimaryAccountNumber = Int64.Parse(paymentCardShortPAN)
            //};
            var paymentCardDictionary = new Dictionary<string, object>();
            paymentCardDictionary.Add("address", /*cardAddress*/paymentCardAddressDictionary);
            paymentCardDictionary.Add("brand", CardType);
            paymentCardDictionary.Add("expiresEnd", String.Format("{0}/{1}", CardExpiryDate.Year, CardExpiryDate.Month));
            paymentCardDictionary.Add("friendlyName", "Dans Card");
            paymentCardDictionary.Add("issueNumber", 1);
            paymentCardDictionary.Add("nameOnCard", "Dan Wagner");
            paymentCardDictionary.Add("primaryAccountNumber", long.Parse(CardNumber));
            paymentCardDictionary.Add("shortPrimaryAccountNumber", int.Parse(CardNumber.Substring(12)));

            //PaymentMethod paymentMethod = new PaymentMethod()
            //{
            //    FriendlyName = paymentMethodFriendlyName,
            //    PaymentType = paymentMethodPaymentType,
            //    PaymentCard = paymentCard
            //};
            var paymentMethodDictionary = new Dictionary<string, object>();
            paymentMethodDictionary.Add("friendlyName", "Dan Wagner");
            paymentMethodDictionary.Add("paymentCard", paymentCardDictionary);
            paymentMethodDictionary.Add("paymentType", "CARD");

            object[] paymentMethodArray = { /*paymentMethod*/paymentMethodDictionary };

            //User user = new User() { Name=username, Gender= gender, DateOfBirth=userDateOfBirthArray, EmailAddress=emailAddress, Locale=deviceLocale, Addresses={address}, PaymentMethods={paymentMethod}, 
            var userDictionary = new Dictionary<string, object>();
            userDictionary.Add("accountNonExpired", 1);
            userDictionary.Add("accountNonLocked", 1);
            userDictionary.Add("addresses", addressArray);
            userDictionary.Add("credentialsNonExpired", 1);
            userDictionary.Add("dateOfBirth", userDateOfBirthArray);
            userDictionary.Add("emailAddress", EmailAddress);
            userDictionary.Add("gender", Gender);
            userDictionary.Add("locale", locale);
            userDictionary.Add("name", Username);
            userDictionary.Add("passphrase", Password);
            userDictionary.Add("paymentMethods", paymentMethodArray);

            var customParameterFields = new Dictionary<string, object>();
            customParameterFields.Add("deviceFingerprint", Utilities.DEVICE_FINGERPRINT);
            customParameterFields.Add("user", userDictionary);
            #endregion

            _requestManager.SendAccountSignupRequest(customParameterFields);
            //TODO: friendly name / name?
            //_requestManager.SendAccountSignupRequest(Username, Gender, DateOfBirth.Year, DateOfBirth.Month, DateOfBirth.Day, EmailAddress, locale, DeliveryAddressName, DeliveryAddressName,
            //    DeliveryAddressLine1, DeliveryAddressLine2, DeliveryAddressLine3, DeliveryAddressLine4, DeliveryAddressCity, DeliveryAddressCounty, DeliveryAddressPostcode,
            //    "232", "GB", "GBR", DeliveryAddressCountry, "826", "My Payment Method", "CARD", "My PaymentCard", CardType, CardNumber, CardNumber.Substring(12), 1, "John Doe", String.Format("{0}/{1}", CardExpiryDate.Month, CardExpiryDate.Year), PaymentAddressName, PaymentAddressName, //TODO:Friendly name / name
            //    PaymentAddressLine1, PaymentAddressLine2, PaymentAddressLine3, PaymentAddressLine4, PaymentAddressCity, PaymentAddressCounty, PaymentAddressPostcode, 232, "GB", "GBR", PaymentAddressCountry, 826, Utilities.DEVICE_FINGERPRINT, Password);

            //TODO: Gender!

            //WORKS!
            //        _requestManager.SendAccountSignupRequest("PowaTag Developer", "M", 1975, 5, 7, "elvis@powatag.com", "en_GB", "Dan Wagner", "Work Address",
            //"Powa Technologies Limited", "9th Floor", "The Met Building", "22 Percy Street", "London", "Greater London", "W1T 2BU",
            //232, "GB", "GBR", "United Kingdom", 826, "Dans Visa Card", "CARD", "Dans Card", "VISA", 4111111111111111, 1111, 1, "Dan Wagner", "2015/01", "Work Address", "Dan Wagner", //TODO:Friendly name / name
            //"Powa Technologies", "9th Floor", "The Met Building", "22 Percy Street", "London", "Greater London", "W1T 2BU", 232, "GB", "GBR", "United Kingdom", 826, "82cfb85953445fe1f40d5fd23065a4766e9491de5e9d1b30769e76903ca3fe62", "P0w4T4g?");
        }

        internal void VerifyEmail(string p)
        {
            _requestManager.EmailVerification(p);
        }
    }
}
