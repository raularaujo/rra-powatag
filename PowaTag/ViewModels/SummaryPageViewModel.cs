﻿using Newtonsoft.Json;
using PowaTag.Models;
using PowaTag.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.ViewModels
{
    public class SummaryPageViewModel : ViewModelBase
    {
        #region Properties
        private System.Windows.Visibility _progressVisibility;
        /// <summary>
        /// Toggles the visibility of the loading screen
        /// </summary>
        public System.Windows.Visibility ProgressVisibility
        {
            get { return _progressVisibility; }
            set
            {
                _progressVisibility = value;
                NotifyPropertyChanged();
            }
        }
        private System.Windows.Visibility _contentVisibility;
        /// <summary>
        /// Toggles the visibility of the main view content.
        /// </summary>
        public System.Windows.Visibility ContentVisibility
        {
            get { return _contentVisibility; }
            set
            {
                _contentVisibility = value;
                NotifyPropertyChanged();
            }
        }

        private Product _product { get { return App.OrderModel.Order.Products.First(); } }
        /// <summary>
        /// Product bound properties
        /// </summary>
        public string ProductImage { get { return _product.ProductImages.First().Url; } }
        public string Name { get { return _product.Name.Replace("&pound;", "£"); } }
        public string Price { get { return _product.Price; } }
        public string ShopName { get { return App.OrderModel.Order.Shop.Name; } }
        public List<ItemDetailViewModel> ProductAttributes = new List<ItemDetailViewModel>();
        public List<ItemDetailViewModel> ProductOptions = new List<ItemDetailViewModel>();

        public IEnumerable<Address> DeliveryAddresses { get { return App.OrderModel.Order.User.Addresses; } }
        private Address _selectedDeliveryAddress;
        public Address SelectedDeliveryAddress
        {
            get { return _selectedDeliveryAddress; }
            set { _selectedDeliveryAddress = value; }
        }

        public IEnumerable<PaymentMethod> PaymentMethods { get { return App.OrderModel.Order.User.PaymentMethods; } }
        private PaymentMethod _selectedPaymentMethod;
        public PaymentMethod SelectedPaymentMethod
        {
            get { return _selectedPaymentMethod; }
            set { _selectedPaymentMethod = value; }
        }


        private ObservableCollection<string> _imageList = new ObservableCollection<string>();
        /// <summary>
        /// List of Image URLs for the ImageRotatorControl
        /// </summary>
        public ObservableCollection<string> ImageList
        {
            get { return _imageList; }
            private set { _imageList = value; NotifyPropertyChanged(); }
        }

        /// <summary>
        /// Powa API details
        /// </summary>
        public string REFERENCE_ID; // = "3f91d9de0762103f586f3c8c3b7396b468035afa24e480c30d7e737decc01140"; //always works!
        const string API_KEY = "9c497051-9382-4349-a60b-5f220bb07562";
        const string DEVICE_FINGERPRINT = "82cfb85953445fe1f40d5fd23065a4766e9491de5e9d1b30769e76903ca3fe62";
        const string SECRET_KEY = "ACDEF01234";
        string latitude = "0", longitude = "0";

        /// <summary>
        /// RequestManager to handle the server calls
        /// </summary>
        private RequestClientManager _requestManager;
        private bool _requestDone;
        PowaNavigationService _navigation = new PowaNavigationService();

        #endregion


        public event EventHandler ApplicationBarIconRefresh;
        public event EventHandler ApplicationBarCompletePurchase;

        /// <summary>
        /// ViewModel constructor
        /// </summary>
        public SummaryPageViewModel()
        {
            ProgressVisibility = System.Windows.Visibility.Visible;
            ContentVisibility = System.Windows.Visibility.Collapsed;

            _requestManager = new RequestClientManager("application/json;charset=UTF-8", "gzip", API_KEY, "application/json; charset=UTF-8", DEVICE_FINGERPRINT, "POST");

            //Events for the different calls
            _requestManager.PurchaseStartCompleted += _requestManager_PurchaseStartCompleted;
            _requestManager.PurchaseAbortCompleted += _requestManager_PurchaseAbortCompleted;
            _requestManager.PurchaseCompleteCompleted += _requestManager_PurchaseCompleteCompleted;

            GetCurrentLocation();
        }

        private async void GetCurrentLocation()
        {
            string location = await PowaTag.Geolocation.TagGeolocator.GetCurrentLocation();
            latitude = location.Split(':')[0];
            longitude = location.Split(':')[1];
        }

        #region Request Client Manager
        /// <summary>
        /// Makes a Request to the RequestClientManager
        /// </summary>
        /// <param name="endpoint"></param>
        public void RequestClientManagerRequest(PowaTag.RequestClientManager.PurchaseEndpoints endpoint)
        {
            if (endpoint.Equals(PowaTag.RequestClientManager.PurchaseEndpoints.PurchaseStart))
            {
                if (Utilities.IsNetworkAvailable)
                    _requestManager.SendPurchaseRequest(endpoint, REFERENCE_ID, Utilities.GetHMAC(API_KEY + DEVICE_FINGERPRINT + SECRET_KEY + REFERENCE_ID, SECRET_KEY), latitude, longitude);
                else
                    OnShowMessageBox(new MessageEventArgs(Resources.AppResources.NetworkErrorMessage));
            } 
            else if (endpoint.Equals(PowaTag.RequestClientManager.PurchaseEndpoints.PurchaseAbort))
            {
                if (Utilities.IsNetworkAvailable)
                    _requestManager.SendPurchaseRequest(endpoint, REFERENCE_ID, Utilities.GetHMAC(API_KEY + DEVICE_FINGERPRINT + SECRET_KEY + REFERENCE_ID, SECRET_KEY), latitude, longitude);
                else
                    OnShowMessageBox(new MessageEventArgs(Resources.AppResources.NetworkErrorMessage));
            }
            else if (endpoint.Equals(PowaTag.RequestClientManager.PurchaseEndpoints.PurchaseAbort))
            {
                if (Utilities.IsNetworkAvailable)
                    _requestManager.SendPurchaseRequest(endpoint, REFERENCE_ID, Utilities.GetHMAC(API_KEY + DEVICE_FINGERPRINT + SECRET_KEY + REFERENCE_ID, SECRET_KEY), latitude, longitude);
                else
                    OnShowMessageBox(new MessageEventArgs(Resources.AppResources.NetworkErrorMessage));
            }
        }

        /// <summary>
        /// Handles the Purchase Complete response
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _requestManager_PurchaseCompleteCompleted(object sender, WebResponseArgs e)
        {
            System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                ProgressVisibility = System.Windows.Visibility.Collapsed;
                _navigation.NavigateTo(new Uri("/Views/ReceiptView.xaml", UriKind.Relative));
            }); ;
        }

        /// <summary>
        /// Handles the Purchase Abort response
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _requestManager_PurchaseAbortCompleted(object sender, WebResponseArgs e)
        {
            // MessageBox.Show("PurchaseAbort complete");
        }

        /// <summary>
        /// Handles the Purchase Start response
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _requestManager_PurchaseStartCompleted(object sender, WebResponseArgs e)
        {
            if (!_requestDone)
            {
                if (!String.IsNullOrEmpty(e.Response))
                {
                    try
                    {
                        RootObject o = JsonConvert.DeserializeObject<RootObject>(e.Response);

                        SetOrderData(o);

                        int index = App.OrderModel.Order.User.FavouriteDeliveryAddress.Id;
                        SelectedDeliveryAddress = App.OrderModel.Order.User.Addresses.ElementAt(index >= App.OrderModel.Order.User.Addresses.Count() ? index - 1 : index);

                        index = App.OrderModel.Order.User.FavouritePaymentMethod.Id;
                        SelectedPaymentMethod = App.OrderModel.Order.User.PaymentMethods.ElementAt(index >= App.OrderModel.Order.User.PaymentMethods.Count() ? index - 1 : index);

                        Utilities.CPA_ENDPOINT = o.PurchaseAbortEndpoint + Utilities.ENDPOINT_QUERYSTIRNG;
                        Utilities.CPC_ENDPOINT = o.PurchaseCompleteEndpoint + Utilities.ENDPOINT_QUERYSTIRNG;
                        _requestDone = true;
                    }
                    catch
                    {
                        //MessageBox.Show("Sorry, an error has occurred");
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Fills in the data of the order
        /// </summary>
        /// <param name="root"></param>
        /// <param name="sv"></param>
        public void SetOrderData(RootObject root)
        {
            App.OrderModel = root;

            foreach (var item in _product.ProductImages.Where(i => !String.IsNullOrEmpty(i.Url)).OrderBy(i => i.SortPos))
            {
                ImageList.Add(item.Url);
            }

            ProductAttributes.Clear();
            ProductOptions.Clear();

            // set all the viewmodels for the product options/attributes
            foreach (SelectableBase item in ((IEnumerable<SelectableBase>)_product.ProductAttributes))
                ProductAttributes.Add(new ItemDetailViewModel(item));

            foreach (SelectableBase item in ((IEnumerable<SelectableBase>)_product.ProductOptions))
                ProductOptions.Add(new ItemDetailViewModel(item));

            System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                ContentVisibility = System.Windows.Visibility.Visible;
                ProgressVisibility = System.Windows.Visibility.Collapsed;
            });
        }

        public void CompletePurchase()
        {
            if (Utilities.IsNetworkAvailable)
            {
                if (this.SelectedDeliveryAddress != null && this.SelectedPaymentMethod != null)
                {
                    this.ProgressVisibility = System.Windows.Visibility.Visible;

                    //var productOptionName = new Dictionary<string, string>();
                    var productCode = new Dictionary<string, ProductOptionValue>();
                    var productOption = new Dictionary<string, Dictionary<string, ProductOptionValue>>(); // ... I know..

                    //Temp going to verification until design decision
                    powaNavigationService.NavigateTo(new Uri(String.Format("/Views/CvvVerificationView.xaml?address={0}&payment={1}", this.SelectedDeliveryAddress.Id.ToString(), this.SelectedPaymentMethod.Id.ToString()), UriKind.Relative));
                    // NavigationService.Navigate(new Uri(String.Format("/Views/CvvVerificationView.xaml?address={0}&payment={1}", _vm.SelectedDeliveryAddress.Id.ToString(), _vm.SelectedPaymentMethod.Id.ToString()), UriKind.Relative));
                    //Use String.Format as shown above.
                    //NavigationService.Navigate(new Uri("/Views/CvvVerificationView.xaml?address="+_vm.SelectedDeliveryAddress.Id.ToString()+"&payment="+_vm.SelectedPaymentMethod.Id.ToString(), UriKind.Relative));


                    this.ProgressVisibility = System.Windows.Visibility.Visible;
                }
                else
                    // MessageBox.Show("Please ensure all options have been set");
                    OnShowMessageBox(new MessageEventArgs("Please ensure all options have been set"));
            }
            else
            {
                //MessageBox.Show(AppResources.NetworkErrorMessage);
                OnShowMessageBox(new MessageEventArgs(PowaTag.Resources.AppResources.NetworkErrorMessage));
            }
  
        }

        /// <summary>
        /// Fills out the data after calling the web service
        /// </summary>
        public void UpdateData(System.Windows.Navigation.NavigationContext nc)
        {
            //Extract the tag url from the navigation           
            if (nc.QueryString.ContainsKey("tagurl"))
            {
                Utilities.REFERENCE_ID = nc.QueryString["tagurl"];

                this.REFERENCE_ID = nc.QueryString["tagurl"];


                this.RequestClientManagerRequest(RequestClientManager.PurchaseEndpoints.PurchaseStart);
                
            }
        }
    }
}
