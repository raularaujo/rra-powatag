﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowaTag.ViewModels
{
    public class SupportViewModel : ViewModelBase
    {
        private string _header = "Support";
        public string Header
        {
            get { return _header; }
            set
            {
                if (_header != value)
                {
                    _header = value;
                    NotifyPropertyChanged("Header");
                }
            }
        }


    }
}
