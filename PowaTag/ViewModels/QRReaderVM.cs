﻿using com.google.zxing;
using PowaTag.Controls;
using PowaTag.Models;
using PowaTag.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PowaTag.ViewModels
{
    public class QRReaderVM : ViewModelBase
    {

        public ICommand ScanCompleteCommand { get; set; }
        public event EventHandler<ScanCompleteEventArgs> QRScanned;

        private bool _canScan;

        public QRReaderVM()
        {
            //allow scanning when this VM is instantiated
            _canScan = true;
            ScanCompleteCommand = new DelegateCommand(ScanComplete, CanScan);
        }


        /// <summary>
        /// Once a Tag is read successfully, this method is run. It will receive the ScanCompleteEventArgs object which contains the result of the Tag.
        /// </summary>
        /// <param name="param">This param should be of type ScanCompleteEventArgs which contains the Tag data</param>
        private void ScanComplete(object param)
        {
            if (Utilities.IsNetworkAvailable)
            {
                ScanCompleteEventArgs args = param as ScanCompleteEventArgs;

                if (args != null)
                {
                    if (QRScanned != null)
                        QRScanned(null, args);

                    string[] split = args.FullResult.Text.Split('/');

                    Utilities.CPS_ENDPOINT = args.FullResult.Text.Substring(0, args.FullResult.Text.IndexOf(split[split.Length - 1]) - 1) + Utilities.ENDPOINT_QUERYSTIRNG;
                    //disable the scanner as it's already found the item
                    _canScan = false;


                    //using the inherited NavigationService to navigate
                    powaNavigationService.NavigateTo(new Uri("/Views/SummaryView.xaml?tagurl=" + split[split.Length - 1].ToString(), UriKind.Relative));
                }
            }
            else
            {
                OnShowMessageBox(new MessageEventArgs(Resources.AppResources.NetworkErrorMessage));
                
                
            }
        }

        /// <summary>
        /// CanScan determines whether we can accept a reading or not (essentailly enables/disables the QR reader)
        /// </summary>
        /// <param name="param">Param can be any object passed in via the XAML binding</param>
        /// <returns></returns>
        private bool CanScan(object param)
        {
            return _canScan;
        }
    }
}
