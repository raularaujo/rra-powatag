using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PowaTag.Resources;
using com.google.zxing.qrcode.encoder;
using PowaTag.ViewModels;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Windows.Media;

namespace PowaTag
{

    public partial class MainPage : PhoneApplicationPage
    {
        private QRReaderVM _qrVM;
        
        // Constructor
        public MainPage()
        {
            InitializeComponent();
      
         
             
            this.Loaded += (s, e) =>
            {
                //Create a new instance of the reader so that it doesn't cache when we reload
                _qrVM = new QRReaderVM();               
       
                _qrVM.ShowMessageBox += _qrVM_ShowMessageBox;
                _qrVM.QRScanned += QRScan_Complete;
                this.DataContext = _qrVM; 
            };


            //TEMPORARY TO TRY AND FIX THE SIGNUP API
           // RequestClientManager rcm = new RequestClientManager("application/json;charset=UTF-8", "gzip", PowaTag.Utility.Utilities.API_KEY, "application/json; charset=UTF-8", PowaTag.Utility.Utilities.DEVICE_FINGERPRINT, "POST");

            //rcm.SendAccountSignupRequest("John Doe", "M", 2000, 1, 1, "john@powa.com", "en_GB", "My Delivery Address", "John's Address",
            //                       "line1", "line2", "line3", "line4", "London", "Greater London", "W1T 2BU", "232", "GB", "GBR",
            //                       "United Kingdom", "826", "My Payment Method", "CARD", "My Payment Card", "VISA", "4444333322221111",
            //                       "1111", 1, "Mr. John Doe", "2015/01", "My Card Address", "johns Card address", "line1", "line2",
            //                       "line3", "line4", "London", "Greater London", "W1T 2BU", 232, "GB", "GBR", "United Kingdom",
            //                       826, PowaTag.Utility.Utilities.DEVICE_FINGERPRINT, "john1234");

            #region Flash Animation storyboard            
            //Create the fade out animation for the flash
            Duration duration = new Duration(new TimeSpan(0, 0, 0, 0, 2500));
            _sbFlash = new Storyboard();            
            _sbFlash.Duration = duration;             
            _sbFlash.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath("Opacity"));
            DoubleAnimation animation = new DoubleAnimation();
            animation.To = 0.0;
            _sbFlash.Children.Add(animation);
            Storyboard.SetTarget(animation, rectFlash);
            #endregion

            FadeInImage();

        }

      

        void _qrVM_ShowMessageBox(object sender, MessageEventArgs e)
        {
            MessageBox.Show(e.Message);
            //Since a messagebox is shown, a network error must've occurred (this may change later if more features are required)
            //Therefore restart the scanning
            qrScanner.StartScanning();
        }

        private Storyboard _sbFlash = null;
        void QRScan_Complete(object sender, Controls.ScanCompleteEventArgs e)
        {
            //show the flash rectangle to give the effect of the screen flashing and then fade it out with the storyboard
            rectFlash.Visibility = System.Windows.Visibility.Visible;
            _sbFlash.Begin();
        }

       

        //OnNavigatedTo is overridden to reinitialize the camera when the page is loaded (normally an issue if the user returns using the backkey and/or from tombstone)
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
             
            try
            {
                //Initialize the camera and set the video brush.
                qrScanner.OnApplyTemplate();

                if (e.NavigationMode != NavigationMode.New)
                {
                    //Timer to only show the application bar once the video brush has loaded so that the user can't select a page whilst the videobrush is empty (causes crash)
                    DateTime dt = DateTime.Now;
                    while (DateTime.Now.Subtract(dt) < TimeSpan.FromMilliseconds(1000))
                        ApplicationBar.IsVisible = false;

                    ApplicationBar.IsVisible = true;
                }
            }
            catch
            { }

            base.OnNavigatedTo(e);
        }

        //OnNavigatingFrom is overriden because we need to dispose the camera as per the MSDN docs (otherwise the camera isn't usable from other apps and/or when user returns to page)
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            //important because we need to "release" the camera so that it's accessible elsewhere.
            //This was a method I added manually into the QRScanner source that resets the important values
            //OnApplyTemplate must be called when the camera should be reinitialized
            try
            {
                qrScanner.StopScanning();
                qrScanner.Dispose();
            }
            catch
            {                
               //Handle any disposing errors
            }


            base.OnNavigatingFrom(e);
        }

        #region ApplicationBar Events
        
        private void MenuOrderHistory_Click(object sender, EventArgs e)
        {
            StopScanAndNavigate("/Views/HistoryView.xaml");          
        }

        private void MenuAccount_Click(object sender, EventArgs e)
        {     
           // NavigationService.Navigate(new Uri("/Views/AccountView.xaml", UriKind.Relative));
            StopScanAndNavigate("/Views/AccountView.xaml");
        }

        private void MenuAbout_Click(object sender, EventArgs e)
        {            
            //NavigationService.Navigate(new Uri("/Views/AboutView.xaml", UriKind.Relative));       
            StopScanAndNavigate("/Views/AboutView.xaml");
        }

        private void MenuSupport_Click(object sender, EventArgs e)
        {            
            //NavigationService.Navigate(new Uri("/Views/SupportView.xaml", UriKind.Relative));         
            //NavigationService.Navigate(new Uri("/Views/SignUpView.xaml", UriKind.Relative));
            StopScanAndNavigate("/Views/SupportView.xaml");

        }

        private void StopScanAndNavigate(string page)
        {
            //stop the scanning before navigating as otherwise the exceptions caught from scanning delays the navigation
            try
            {
                qrScanner.StopScanning();
                NavigationService.Navigate(new Uri(page, UriKind.Relative));
            }
            catch
            {

            }           
        }
        #endregion

        private void FadeInImage()
        {
            ApplicationBar.IsVisible = false;
            DoubleAnimation fadeIn = new DoubleAnimation { From = 1, To = 0, BeginTime=TimeSpan.FromSeconds(1), Duration = TimeSpan.FromSeconds(1) };
            Storyboard.SetTarget(fadeIn, SplashScreen);
            Storyboard.SetTargetProperty(fadeIn, new PropertyPath(PowaTag.Controls.QRCodeScanner.OpacityProperty));

            var sb = new Storyboard();
            sb.Children.Add(fadeIn);
            sb.Completed += (s, e) =>
            {
                ApplicationBar.IsVisible = true;
            };
            sb.Begin();
        }

        private void MenuSignUp_Click(object sender, EventArgs e)
        {
            StopScanAndNavigate("/Views/SignUpView.xaml");
        }
    }
}
