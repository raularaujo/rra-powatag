﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using PowaTag.ViewModels;
using PowaTag.Models;
using PowaTag.Utility;

namespace PowaTag.Views
{
    public partial class AccountView : PhoneApplicationPage
    {       

        private AccountPageViewModel _accountDataModel;
        private int _timeOlder = 30;

        //RCM used to communicate with the server
        private RequestClientManager _rcm;
        private bool _requestDone = false;

        public AccountView()
        {
            InitializeComponent();
            //New Client manager instance to call the history request.
            _rcm = new RequestClientManager("application/json;charset=UTF-8", "gzip", Utilities.API_KEY, "application/x-www-form-urlencoded", Utilities.DEVICE_FINGERPRINT, "GET");
            _rcm.ResponseReturned += rcm_ResponseReturned;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            _requestDone = false;
            //Start Loading data, either from Server or the local storage.
            StartAccountViewDataLoad();
        }

        /// <summary>
        /// This method starts the process for loading the data to the Account View.
        /// Data can come from the server, or could be previously stored in the local storage system.
        /// Data is loaded again from server if it's older than a certain quantity of time.
        /// </summary>
        void StartAccountViewDataLoad()
        {
            DateTime timeStamp;
            string timeStampSetting;
            TimeSpan timeDifference;

            //Check phones connectivity
            if (Utilities.IsNetworkAvailable)
            {
                //Check how old the Account info is.
                if (StorageManager.CheckSetting(Utilities.AccountTimeStampSetting))
                {
                    timeStampSetting = (string)StorageManager.ReadSettingFromSettings(Utilities.AccountTimeStampSetting);

                    if (timeStampSetting != null)
                    {
                        //Parse previous TimeStamp
                        DateTime.TryParse(timeStampSetting, out timeStamp);
                        //Do the Time difference.
                        timeDifference = DateTime.UtcNow.Subtract(timeStamp);
                        //Older than X time ?
                        if (timeDifference.Minutes > _timeOlder)
                            _rcm.GenerateAccountRequest();
                        else
                            LoadAccountDataFromIsolatedStorage();
                    }
                    else
                        _rcm.GenerateAccountRequest();
                }
                else
                    _rcm.GenerateAccountRequest();
            }
            else
            {
                //If data is already stored.
                if (StorageManager.CheckSetting(Utilities.AccountTimeStampSetting))
                {
                    LoadAccountDataFromIsolatedStorage();
                }
            }
        }

        private void LoadAccountDataFromIsolatedStorage()
        {
            try
            {
                Debug.WriteLine("\n@AccountView.xaml [LoadAccountInfoFromIsolatedStorage] Loading Account Info from IsoStorage");

                string content = StorageManager.ReadFileFromStorage(Utilities.AccountDataFileName);
                if (content != string.Empty)
                {
                    SetDataToAccountView(content);
                }
            }
            catch (Newtonsoft.Json.JsonException ex)
            {
                _requestDone = false;
                Debug.WriteLine("\n@HistoryView.xaml [rcm_ResponseReturned] JsonDeserializer Exception " + ex.Message);
            }
        }

        void SetDataToAccountView(string data)
        {
            try
            {
                AccountRootObject _accountJsonData = Newtonsoft.Json.JsonConvert.DeserializeObject<AccountRootObject>(data);
                _accountDataModel = new AccountPageViewModel()
                {
                    EmailAddressValue = _accountJsonData.user.EmailAddress,
                    UsernameValue = _accountJsonData.user.Name,

                    BillingNameValue = _accountJsonData.user.Addresses.First().Name,
                    BillingAddressValue = _accountJsonData.user.Addresses.First().Line1,
                    BillingTownCityValue = _accountJsonData.user.Addresses.First().City,
                    BillingCountyValue = _accountJsonData.user.Addresses.First().County,
                    BillingPostcodeValue = _accountJsonData.user.Addresses.First().PostCode,
                    BillingCountryValue = _accountJsonData.user.Addresses.First().Country.Name,

                    DeliveryNameValue = _accountJsonData.user.FavouriteDeliveryAddress.Name,
                    DeliveryAddressValue = _accountJsonData.user.FavouriteDeliveryAddress.Line1,
                    DeliveryTownCityValue = _accountJsonData.user.FavouriteDeliveryAddress.City,
                    DeliveryCountyValue = _accountJsonData.user.FavouriteDeliveryAddress.County,
                    DeliveryCountryValue = _accountJsonData.user.FavouriteDeliveryAddress.Country.Name,
                    DeliveryPostcodeValue = _accountJsonData.user.FavouriteDeliveryAddress.PostCode,

                    CardExpiryDateValue = _accountJsonData.user.FavouritePaymentMethod.PaymentCard.ExpiresEnd,
                    CardNumberValue = (string)_accountJsonData.user.FavouritePaymentMethod.PaymentCard.PrimaryAccountNumber,
                    CardTypeValue = _accountJsonData.user.FavouritePaymentMethod.PaymentType
                };
                _requestDone = true;
                SetDataContext();
            }
            catch (Newtonsoft.Json.JsonException ex)
            {
                _requestDone = false;
                Debug.WriteLine("\n@HistoryView.xaml [SetDataToAccountView] JsonDeserializer Exception " + ex.Message);
            }
        }

        void rcm_ResponseReturned(object sender, WebResponseArgs e)
        {
            try
            {
                Dispatcher.BeginInvoke(() =>
                {
                    
                        Debug.WriteLine("\n@AccountView.xaml [rcm_ResponseReturned] Response returned, Deserializing object");
                        
                        SetDataToAccountView(e.Response);
                        
                        //Update the TimeStamp for this Response.
                        StorageManager.WriteSettingToSettings(Utilities.AccountTimeStampSetting, DateTime.UtcNow.ToShortTimeString());
                        //Write new Data to the old file.
                        StorageManager.WriteFileToStorage(Utilities.AccountDataFileName, e.Response);
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\n@AccountView.xaml [rcm_ResponseReturned] Exception fired: " + ex.Message);
                _requestDone = false;
            }
        }

        public void SetDataContext()
        {
            if (_accountDataModel != null && _requestDone)
            {
                this.DataContext = _accountDataModel;   
            }
        }
    }
}