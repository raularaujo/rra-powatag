﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using PowaTag.ViewModels;
using System.Diagnostics;
using PowaTag.Models;
using PowaTag.Utility;

namespace PowaTag.Views
{
    public partial class HistoryView : PhoneApplicationPage
    {
        private int _timeOlder = 1;

        /// <summary>
        /// Is the Data Model that fills the information
        /// </summary>
        private ObservableCollection<HistoryPageViewModel> _historyDataModel;
        
        //RCM used to communicate with the server
        private RequestClientManager _rcm;
        private bool _requestDone = false;
        /// <summary>
        /// Counts the number of pages requested to the server.
        /// </summary>
        private int _pageCount;
        
        /// <summary>
        /// Current date when a History request is made.
        /// </summary>
        private string _date;
        /// <summary>
        /// Size of the page requested. Quantity of items to be retrieved. Set to 10 normaly
        /// </summary>
        private string _pageSize;

        /// <summary>
        /// View on charge of showing the History of every purchase made by the user.
        /// </summary>
        public HistoryView()
        {
            InitializeComponent();
            
            _historyDataModel = new ObservableCollection<HistoryPageViewModel>();
            
            _pageCount = 1;
            _date = string.Empty;
            
            //New Client manager instance to call the history request.
            _rcm = new RequestClientManager("application/json;charset=UTF-8", "gzip", Utilities.API_KEY, "application/x-www-form-urlencoded", Utilities.DEVICE_FINGERPRINT, "GET");
            
            //Event fired when response from the server is received with the history records.
            _rcm.ResponseReturned += rcm_ResponseReturned;
        }

        /// <summary>
        /// Handles the OnNavigatedTo event.
        /// When navigation to this page fromt the QR scanner, sends a CPS 
        /// request to the server.
        /// Navigating to this page should include the REFERENCE_ID in a tagurl querystring.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (e.NavigationMode != NavigationMode.Back)
            {
                //TODO For History Requests, RefId is not needed. Delete if-else, send RefId empty string.
                if (this.NavigationContext.QueryString.ContainsKey("tagurl"))
                    Utilities.REFERENCE_ID = this.NavigationContext.QueryString["tagurl"];
                else
                    Utilities.REFERENCE_ID = string.Empty;

                _requestDone = false;
                StartHistoryViewDataLoad();
                
            }
        }

        /// <summary>
        /// This method starts the process for loading the data to the History View.
        /// Data can come from the server, or could be previously stored in the local storage system.
        /// Data is loaded again from server if it's older than a certain quantity of time.
        /// </summary>
        void StartHistoryViewDataLoad()
        {
            DateTime timeStamp;
            string timeStampSetting;
            TimeSpan timeDifference;

            //Set parameters for data Request
            _date = DateTime.Now.ToString("ddd, d MMM yyyy HH:mm:ss") + " GMT";
            _pageCount = 1;
            _pageSize = "10";

            Debug.WriteLine("\n@HistoryView.xaml [OnNavigatedTo] Date value is: " + _date);

            //Check phones connectivity
            if (Utilities.IsNetworkAvailable)
            {
                //Check how old the Account info is.
                if (StorageManager.CheckSetting(Utilities.HistoryTimeStampSetting))
                {
                    timeStampSetting = (string)StorageManager.ReadSettingFromSettings(Utilities.HistoryTimeStampSetting);

                    if (timeStampSetting != null)
                    {
                        //Parse previous TimeStamp
                        DateTime.TryParse(timeStampSetting, out timeStamp);
                        //Do the Time difference.
                        timeDifference = DateTime.UtcNow.Subtract(timeStamp);
                        //Older than X time ?
                        if (timeDifference.Days > _timeOlder)
                            _rcm.GenerateHistoryRecordsRequest(_date, _pageCount.ToString(), _pageSize);
                        else
                            LoadHistoryDataFromIsolatedStorage();
                    }
                    else
                        _rcm.GenerateHistoryRecordsRequest(_date, _pageCount.ToString(), _pageSize);
                }
                else
                    _rcm.GenerateHistoryRecordsRequest(_date, _pageCount.ToString(), _pageSize);
            }
            else
            {
                MessageBox.Show(PowaTag.Resources.AppResources.NetworkErrorMessage);
                //If data is already stored.
                if (StorageManager.CheckSetting(Utilities.HistoryTimeStampSetting))
                {
                    LoadHistoryDataFromIsolatedStorage();
                }
            }
        }

        void SetDataToHistoryView(string data)
        {
            string productName, purchaseDate, partialDate, productPrice, productImageUrl;
            try
            {
                HistoryRootObject historyRecords = Newtonsoft.Json.JsonConvert.DeserializeObject<HistoryRootObject>(data);

                //Get Products Information
                foreach (HistoryOrder order in (IEnumerable<HistoryOrder>)historyRecords.Orders)
                {
                    foreach (Product product in (IEnumerable<Product>)order.Products)
                    {
                        productName = product.Name;
                        partialDate = DateTime.FromBinary((long)product.CreatedDateTime).ToString("MMM yy");
                        purchaseDate = DateTime.FromBinary((long)product.CreatedDateTime).ToString();
                        productPrice = product.Price;
                        productImageUrl = product.ProductImages.First().Url;

                        _historyDataModel.Add(new HistoryPageViewModel
                        {
                            PartialDate = partialDate,
                            SourceProductImage = productImageUrl,
                            ProductName = productName,
                            ProductPrice = productPrice,
                            PurchaseDate = purchaseDate
                        });
                    }
                }
                _requestDone = true;

                //We save the object in order to be accesible from the ReceiptView
                App.HistoryRecordsModel = historyRecords;

                //***
                // Set the context once the data is parsed and stored to the DataModel
                //***
                SetItemsSourceContext();

            }
            catch (Newtonsoft.Json.JsonException ex)
            {
                _requestDone = false;
                Debug.WriteLine("\n@HistoryView.xaml [SetDataToAccountView] JsonDeserializer Exception " + ex.Message);
            }
        }

        private void LoadHistoryDataFromIsolatedStorage()
        {
            try
            {
                Debug.WriteLine("\n@HistoryView.xaml [LoadAccountInfoFromIsolatedStorage] Loading Account Info from IsoStorage");

                string content = StorageManager.ReadFileFromStorage(Utilities.HistoryDataFileName);
                if (content != string.Empty)
                {
                    SetDataToHistoryView(content);
                }
            }
            catch (Newtonsoft.Json.JsonException ex)
            {
                _requestDone = false;
                Debug.WriteLine("\n@HistoryView.xaml [rcm_ResponseReturned] JsonDeserializer Exception " + ex.Message);
            }
        }

        /// <summary>
        /// Handles the ResponseReturned event from the RequestClientManager.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void rcm_ResponseReturned(object sender, WebResponseArgs e)
        {
            try
            {
                Dispatcher.BeginInvoke(() =>
                {
                    SetDataToHistoryView(e.Response);
                    //Update the TimeStamp for this Response.
                    StorageManager.WriteSettingToSettings(Utilities.HistoryTimeStampSetting, DateTime.UtcNow.ToShortTimeString());
                    //Write new Data to the old file.
                    StorageManager.WriteFileToStorage(Utilities.HistoryDataFileName, e.Response);
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\n@HistoryView.xaml [rcm_ResponseReturned] Exception Trying to Deserialize Json data. " + ex.Message);
                _requestDone = false;
            }
        }

        public void SetItemsSourceContext()
        {
            if (_historyDataModel != null && _requestDone)
            {
                listTagsHistory.ItemsSource = _historyDataModel;
            }
        }
 

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //TODO - Check if this will change
            _date = DateTime.Now.ToString("ddd, d MMM yyyy HH:mm:ss") + " GMT";
            if (Utilities.IsNetworkAvailable)
                _rcm.GenerateHistoryRecordsRequest(_date, (_pageCount++).ToString(), "10");
            else                
                MessageBox.Show(PowaTag.Resources.AppResources.NetworkErrorMessage);
        }

        private void HistoryList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {            
            int selectedIndex = listTagsHistory.SelectedIndex;
            NavigationService.Navigate(new Uri("/Views/ReceiptView.xaml?productindex=" + selectedIndex, UriKind.Relative));
            Debug.WriteLine("\n@HistoryView.xaml [HistoryList_SelectionChanged] Tap on item:  " + selectedIndex);
        }
    }
}