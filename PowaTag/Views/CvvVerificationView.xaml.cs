﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PowaTag.ViewModels;

namespace PowaTag.Views
{
    public partial class CvvVerificationView : PhoneApplicationPage
    {
        private CvvVerificationViewModel _vm = null;
        public CvvVerificationView()
        {
            InitializeComponent();
            if (!System.ComponentModel.DesignerProperties.IsInDesignTool)
            {
                _vm = new CvvVerificationViewModel();


                _vm.ShowMessageBox += _vm_ShowMessageBox;
                this.DataContext = _vm;
                _vm.UIDispatcher = this.Dispatcher;

                //auto select the textbox
                Loaded += (s, a) => { cvv1.Focus(); };
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (this.NavigationContext.QueryString.ContainsKey("address"))
            {
                _vm.addressId = this.NavigationContext.QueryString["address"];
            }
            if (this.NavigationContext.QueryString.ContainsKey("payment"))
            {
                _vm.paymentId = this.NavigationContext.QueryString["payment"];
            }
        }


        void _vm_ShowMessageBox(object sender, MessageEventArgs e)
        {
            MessageBox.Show(e.Message);
        }

        private void cvv1_PasswordChanged1(object sender, RoutedEventArgs e)
        {
            //if(cvv1.Password.Length > 0)
            //    cvv2.Focus();
            //}
            //private void cvv1_PasswordChanged2(object sender, RoutedEventArgs e)
            //{
            //    if (cvv2.Password.Length > 0)
            //        cvv3.Focus();
            //}
            //private void cvv1_PasswordChanged3(object sender, RoutedEventArgs e)
            //{
            //    if (cvv3.Password.Length > 0)
            //        this.Focus();   
            //}
        }

        string _enteredPasscode = "";
        string _passwordChar = "●";
        private void cvv1_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (_enteredPasscode.Length >= 6) return;
            var PasswordTextBox = sender as TextBox;

            //modify new passcode according to entered key	
            _enteredPasscode = GetNewPasscode(_enteredPasscode, e.PlatformKeyCode);

            //replace text by *	
            PasswordTextBox.Text = System.Text.RegularExpressions.Regex.Replace(_enteredPasscode, @"[^ ]", _passwordChar);
            //take cursor to end of string	
            PasswordTextBox.SelectionStart = PasswordTextBox.Text.Length;

            _vm.Pin = _enteredPasscode.Replace(" ", "");
        }
        private string GetNewPasscode(string oldPasscode, int keyId)
        {
            string newPasscode = string.Empty;
            switch (keyId)
            {
                case 8:
                    //back key pressed			
                    if (oldPasscode.Length > 0)
                        newPasscode = oldPasscode.Substring(0, oldPasscode.Length - 2);
                    break;
                case 190:
                    // . pressed			
                    newPasscode = oldPasscode;
                    break;
                default:
                    //Number pressed			
                    newPasscode = oldPasscode + (keyId - 48) + " "; break;
            }

            return newPasscode;
        }
    }
}