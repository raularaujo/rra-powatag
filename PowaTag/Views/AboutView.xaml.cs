﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace PowaTag.Views
{
    public partial class AboutView : PhoneApplicationPage
    {
        ViewModels.AboutViewModel aboutViewModel;
        public AboutView()
        {
            InitializeComponent();
            

            this.Loaded += AboutView_Loaded;
        }

        //TODO Clear Hardcoded Data!
        void AboutView_Loaded(object sender, RoutedEventArgs e)
        {
            aboutViewModel = new ViewModels.AboutViewModel();
            
                aboutViewModel.Version = System.Xml.Linq.XDocument.Load("WMAppManifest.xml").Root.Element("App").Attribute("Version").Value;
                aboutViewModel.Build = Utility.Utilities.Build;
                aboutViewModel.Date = Utility.Utilities.BuildDate; 
                aboutViewModel.Web = Utility.Utilities.PowaTagUrl;
                aboutViewModel.Facebook = Utility.Utilities.PowaFacebook;
                aboutViewModel.Twitter = Utility.Utilities.PowaTwitter;
                
            

            LayoutRoot.DataContext = aboutViewModel;
            
        }
    }
}