﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PowaTag.ViewModels;

namespace PowaTag.Views
{
    public partial class SignUpView : PhoneApplicationPage
    {
        SignUpViewModel _vm;

        public SignUpView()
        {
            InitializeComponent();

            txtEmail.LostFocus += txtEmail_LostFocus;

            _vm = new SignUpViewModel();
            DataContext = _vm;
        }

        void txtEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            _vm.VerifyEmail(txtEmail.Text);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _vm.RequestSignUp();
        }

    }
}