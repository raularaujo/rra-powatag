﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Maps.Controls;
using System.Windows.Media.Imaging;
using PowaTag.Utility;
using System.Device.Location;

namespace PowaTag.Views
{
    public partial class MapView : PhoneApplicationPage
    {
        public MapView()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (this.NavigationContext.QueryString.ContainsKey("coordinates"))
            {
                string location = this.NavigationContext.QueryString["coordinates"];
                
                double latitude = Convert.ToDouble(location.Split(',')[0]);
                double longitude = Convert.ToDouble(location.Split(',')[1]);

                mapPurchaseLocation.Center = new GeoCoordinate(latitude, longitude);
                mapPurchaseLocation.ZoomLevel = double.Parse(Utilities.ZoomLevel);

                MapOverlay overlay = new MapOverlay
                {
                    GeoCoordinate = mapPurchaseLocation.Center,
                    Content = new Image
                    {
                        Source = new BitmapImage(new Uri("/Assets/Images/map_marker.png", UriKind.Relative)),
                        Stretch = System.Windows.Media.Stretch.Fill,
                        Width = 77,
                        Height = 75,
                    },
                    PositionOrigin = Utilities.MapMarkerOrigin
                };
                MapLayer layer = new MapLayer();
                layer.Add(overlay);
                mapPurchaseLocation.Layers.Add(layer);
            }
        }
    }
}