﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PowaTag.ViewModels;

namespace PowaTag.Views
{
    public partial class ProductImageView : PhoneApplicationPage
    {
        ProductImageViewModel _vm = new ProductImageViewModel();


        public ProductImageView()
        {
            InitializeComponent();

            DataContext = _vm;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (this.NavigationContext.QueryString.ContainsKey("imageurl"))
            {
                var str = this.NavigationContext.QueryString["imageurl"].Split('%');

                    _vm.AddImages(str);
            }
        }
    }
}