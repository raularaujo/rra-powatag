﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace PowaTag.Views
{
    public partial class SupportView : PhoneApplicationPage
    {
        public SupportView()
        {
            InitializeComponent();
            webBrowserSupport.Loaded += webBrowserSupport_Loaded;
            webBrowserSupport.Navigated += webBrowserSupport_Navigated;
        }

        void webBrowserSupport_Navigated(object sender, NavigationEventArgs e)
        {
            progressBar.Visibility = System.Windows.Visibility.Collapsed;
        }


        void webBrowserSupport_Loaded(object sender, RoutedEventArgs e)
        {
            webBrowserSupport.Navigate(new Uri(Utility.Utilities.PowaTagUrl, UriKind.RelativeOrAbsolute));
        }

    }
}