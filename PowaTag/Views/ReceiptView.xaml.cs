﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Maps.Controls;
using System.Device.Location;
using Windows.Devices.Geolocation;
using PowaTag.Models;
using System.IO;
using Newtonsoft.Json;
using PowaTag.Geolocation;
using System.Windows.Media.Imaging;
using PowaTag.Utility;

namespace PowaTag
{
    public partial class ReceiptView : PhoneApplicationPage
    {
        private Order _order;
        private Product _product;
        private int _productIndex;
        private bool _deviceLocated;

        MapManager mapMngr;

        OrderDataModel _orderReceipt;
        
        

        public ReceiptView()
        {
            InitializeComponent();

            _orderReceipt = new OrderDataModel();
            
            mapMngr = new MapManager();

            mapMngr.GetCurrentLocation();
            mapMngr.ImageryMapLocationEvent += mapMngr_DeviceLocatedEvent;
            
            _orderReceipt.ImageList.CollectionChanged += (s, a) =>
            {
                System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    _imageRotator.Source = _orderReceipt.ImageList;
                });
            };
        }

        //This page is requested
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            
     
            if (Utilities.LastPageInNavigationStack().Contains("CvvVerificationView.xaml"))
            {
                NavigationService.RemoveBackEntry(); //remove verification  page
                if (Utilities.LastPageInNavigationStack().Contains("SummaryView.xaml"))
                {
                    NavigationService.RemoveBackEntry(); //remove the summary page
                }                
            }
    
            _productIndex = -1;
            //User has Navigated from HistoryView.
            if (this.NavigationContext.QueryString.ContainsKey("productindex"))
            {
                _productIndex = Int32.Parse(this.NavigationContext.QueryString["productindex"]);
                SetReceiptInfoFromHistoryView(App.HistoryRecordsModel);
            }
            else
                SetReceiptInfoFromSummaryView(App.OrderModel);
        }

        /// <summary>
        /// This method handles the event fired by the MapManager object, when the device has been located.
        /// Once the device is located, a Imagery Map request is sent.
        /// For more information. http://msdn.microsoft.com/en-us/library/ff701724.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">LocationCompletedEvent which is class that contents a string with string format: "lat,long"</param>
        private void mapMngr_DeviceLocatedEvent(object sender, LocationCompletedArgs e)
        {
            loadImageryMapProcess.Visibility = Visibility.Visible;
            App.Coordinates = e.Location;
            string imagerySource = mapMngr.GetStaticImageryMap(App.Coordinates, App.Coordinates);

            try
            {
                staticImageryMap.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(imagerySource, UriKind.Absolute));
                _deviceLocated = true;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("The location service was not able to find your current position.");
                _deviceLocated = false;
            }
        }

        /// <summary>
        /// Given a RootObject, data is read, asign and binded to the corresponding property.
        /// Info has been previously loaded on the SummaryView
        /// </summary>
        /// <param name="fullOrderInfo"></param>
        public void SetReceiptInfoFromSummaryView(RootObject fullOrderInfo)
        {
            if (fullOrderInfo == null)
                return;

            _order = fullOrderInfo.Order;
            _product = _order.Products.First();

            _orderReceipt.ProductName = _product.Name.Replace("&pound;","£");
            _orderReceipt.ProductDescription = _order.Products.First().Description;
            _orderReceipt.PriceValue = _product.Price;
            _orderReceipt.MerchantValue = _order.Shop.Name;
            _orderReceipt.PaymentMethodValue = _order.PaymentMethod;
            _orderReceipt.CreditCardValue = (string)_order.User.PaymentMethods.First().PaymentCard.PrimaryAccountNumber;
            _orderReceipt.DeliveryAddress = _order.DeliveryAddress;

            //If tiles are not required this sets the product image.
            //_orderReceipt.ProductImage = _order.Products.First().ProductImages.First().Url;

            foreach (var item in _product.ProductImages.Where(i => !String.IsNullOrEmpty(i.Url)).OrderBy(i => i.SortPos)) 
            {
                _orderReceipt.ImageList.Add(item.Url);
            }

            DataContext = _orderReceipt;
            _imageRotator.Source = _orderReceipt.ImageList;
        }

        /// <summary>
        /// This method fills up all the Receipt View when comming from the 
        /// </summary>
        /// <param name="historyRecords"></param>
        public void SetReceiptInfoFromHistoryView(HistoryRootObject historyRecords)
        {
            if (App.HistoryRecordsModel == null)
                return;
            if (_productIndex != -1)
            {
                string deliveryAddress = string.Format("{0}, {1}. {2}, {3}", historyRecords.Orders.ElementAt(_productIndex).DeliveryAddress.Line1,
                    historyRecords.Orders.ElementAt(_productIndex).DeliveryAddress.PostCode, historyRecords.Orders.ElementAt(_productIndex).DeliveryAddress.City,
                    historyRecords.Orders.ElementAt(_productIndex).DeliveryAddress.Country);

                _orderReceipt.ProductName = historyRecords.Orders.ElementAt(_productIndex).Products.First().Name;
                _orderReceipt.ProductDescription = historyRecords.Orders.ElementAt(_productIndex).Products.First().Description;
                _orderReceipt.PriceValue = historyRecords.Orders.ElementAt(_productIndex).Products.First().Price;
                _orderReceipt.MerchantValue = historyRecords.Orders.ElementAt(_productIndex).Shop.Name;
                _orderReceipt.PaymentMethodValue = historyRecords.Orders.ElementAt(_productIndex).PaymentMethod.PaymentType;
                _orderReceipt.CreditCardValue = historyRecords.Orders.ElementAt(_productIndex).PaymentMethod.PaymentCard.NameOnCard;
                _orderReceipt.DeliveryAddress = deliveryAddress;
                //If Product Image Tiles are not needed, this line sets the Product Image
                //ProductImage = historyRecords.Orders.ElementAt(_productIndex).Products.First().ProductImages.First().Url
                _orderReceipt.ImageList.Add(historyRecords.Orders.ElementAt(_productIndex).Products.First().ProductImages.First().Url);
                
                DataContext = _orderReceipt;
                _imageRotator.Source = _orderReceipt.ImageList;
            }
        }

        private void staticImageryMap_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (_deviceLocated)
            {
                loadImageryMapProcess.Visibility = Visibility.Collapsed;
                NavigationService.Navigate(new Uri("/Views/MapView.xaml?coordinates=" + App.Coordinates, UriKind.Relative));
            }
        }

        
    }
}