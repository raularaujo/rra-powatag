﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using PowaTag.Resources;
using PowaTag.Models;
using PowaTag.ViewModels;
using Newtonsoft.Json;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using PowaTag.Utility;

namespace PowaTag.Views
{
    public partial class SummaryView : PhoneApplicationPage
    {
        SummaryPageViewModel _vm;

        public SummaryView()
        {
            InitializeComponent();

            _vm = new SummaryPageViewModel();
            this.DataContext = _vm;
            _vm.ShowMessageBox += _vm_ShowMessageBox;


            _vm.ImageList.CollectionChanged += (s, a) =>
            {
                System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    _imageRotator.Source = _vm.ImageList;
                });
            };
        }



        void _vm_ShowMessageBox(object sender, MessageEventArgs e)
        {
            MessageBox.Show(e.Message);
        }


        /// <summary>
        /// Handles the OnNavigatedTo event.
        /// When navigation to this page fromt the QR scanner, sends a CPS 
        /// request to the server.
        /// Navigating to this page should include the REFERENCE_ID in a tagurl querystring.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            UpdateData();

            System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                _lstItemAttributes.ItemsSource = _vm.ProductAttributes;
                _lstItemOptions.ItemsSource = _vm.ProductOptions;
            });
        }

        /// <summary>
        /// Handles the OnBackKeyPress.
        /// When navigating back from this page sends the CPA request to the 
        /// server, aborting the purchase.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {

            //abort any pending requests
            _vm.RequestClientManagerRequest(RequestClientManager.PurchaseEndpoints.PurchaseAbort);

            base.OnBackKeyPress(e);
        }

        /// <summary>
        /// Handles the AppBar click.
        /// Sends a CPC Request.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            if (Utilities.IsNetworkAvailable)
            {
                if (_vm.SelectedDeliveryAddress != null && _vm.SelectedPaymentMethod != null)
                {
                    _vm.ProgressVisibility = System.Windows.Visibility.Visible;

                    //var productOptionName = new Dictionary<string, string>();
                    var productCode = new Dictionary<string, ProductOptionValue>();
                    var productOption = new Dictionary<string, Dictionary<string, ProductOptionValue>>(); // ... I know..

                    //Temp going to verification until design decision

                    NavigationService.Navigate(new Uri(String.Format("/Views/CvvVerificationView.xaml?address={0}&payment={1}", _vm.SelectedDeliveryAddress.Id.ToString(), _vm.SelectedPaymentMethod.Id.ToString()), UriKind.Relative));
                    //Use String.Format as shown above.
                    //NavigationService.Navigate(new Uri("/Views/CvvVerificationView.xaml?address="+_vm.SelectedDeliveryAddress.Id.ToString()+"&payment="+_vm.SelectedPaymentMethod.Id.ToString(), UriKind.Relative));


                    _vm.ProgressVisibility = System.Windows.Visibility.Visible;
                }
                else
                    MessageBox.Show("Please ensure all options have been set");
            }
            else
            {
                MessageBox.Show(AppResources.NetworkErrorMessage);
            }
            //_vm.CompletePurchase();

        }

        private void ApplicationBarIconButton_Refresh(object sender, EventArgs e)
        {
            UpdateData();
        }

        /// <summary>
        /// Fills out the data after calling the web service
        /// </summary>
        private void UpdateData()
        {
            //Extract the tag url from the navigation
            if (this.NavigationContext.QueryString.ContainsKey("tagurl"))
            {
                Utilities.REFERENCE_ID = this.NavigationContext.QueryString["tagurl"];

                _vm.REFERENCE_ID = this.NavigationContext.QueryString["tagurl"];


                _vm.RequestClientManagerRequest(RequestClientManager.PurchaseEndpoints.PurchaseStart);


                _lstItemAttributes.ItemsSource = _vm.ProductAttributes;
                _lstItemOptions.ItemsSource = _vm.ProductOptions;
                _imageRotator.Source = _vm.ImageList;
            }
            //_vm.UpdateData(this.NavigationContext);
        }


    }
}
