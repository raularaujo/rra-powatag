﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Windows.Devices.Geolocation;

namespace PowaTag.Geolocation
{
    /// <summary>
    /// Class to control and get the current location of the device. Also controls if the users allow the app to get access to it's location.
    /// </summary>
    class TagGeolocator
    {
        /// <summary>
        /// Method to get user's consent to access to his current location.
        /// </summary>
        /// <returns>Boolean value that confirms whether Location is consent by the user</returns>
        protected static bool GetConfirmation()
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains("LocationConsent"))
            {
                return Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings["LocationConsent"]);
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Tag It needs to access to your current location", "Location", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK) 
                {
                    IsolatedStorageSettings.ApplicationSettings["LocationConsent"] = true;
                }
                else
                {
                    IsolatedStorageSettings.ApplicationSettings["LocationConsent"] = false;
                }
                IsolatedStorageSettings.ApplicationSettings.Save();
                return result == MessageBoxResult.OK;
            }

        }

        /// <summary>
        /// Method to get the current location of the user (device).
        /// </summary>
        /// <returns>Latitude and Longitude location separated by a ':' token.</returns>
        public static async Task<string> GetCurrentLocation()
        {
            if (GetConfirmation())
            {
                Geolocator geolocator = new Geolocator();
                geolocator.DesiredAccuracyInMeters = 2;

                try
                {
                    Geoposition geoposition = await geolocator.GetGeopositionAsync(
                        maximumAge: TimeSpan.FromMinutes(2),
                        timeout: TimeSpan.FromSeconds(8)
                    );
             
                    return geoposition.Coordinate.Latitude.ToString() + ":" + geoposition.Coordinate.Longitude.ToString();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Can't get the position. Exception Message: " + e.Message);
                    if ((uint)e.HResult == 0x80004004)
                    {
                        MessageBox.Show("Location seems to be turned off in the phone settings, please turn it on.");
                    }
                    return "0:0";
                }
            }
            else
                return "0:0";
        }
    }
}
